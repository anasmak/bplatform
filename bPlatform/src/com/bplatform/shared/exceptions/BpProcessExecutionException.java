package com.bplatform.shared.exceptions;

public class BpProcessExecutionException extends Exception {
	private static final long serialVersionUID = -938062172062167435L;
	
	private String entityId;
	private String statusName;
	
	public BpProcessExecutionException(String entityId, String statusName) {
		this.statusName = statusName;
		this.entityId = entityId;
	}

	public String getEntityId() {
		return entityId;
	}

	public String getStatusName() {
		return statusName;
	}
	
}
