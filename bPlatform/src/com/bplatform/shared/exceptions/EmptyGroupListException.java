package com.bplatform.shared.exceptions;

/**
 * @author IhorG
 *
 */
public class EmptyGroupListException extends Exception{
	
	public EmptyGroupListException() {
		super();
	}

	public EmptyGroupListException(String s) {
		super(s);
	}

	private static final long serialVersionUID = 1L;

}
