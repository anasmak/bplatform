package com.bplatform.shared.exceptions;

import org.springframework.security.core.AuthenticationException;

public class BpAuthenticationException extends AuthenticationException{
	
	private static final long serialVersionUID = -7074164201597516423l;
	
	public Reason getReason(){
		return reason;
	}
	
	
	// Default unused constructor for serialization
	public BpAuthenticationException(){
		super("Exception was thrown by unresolved issue. ");
		reason = Reason.UNRESOLVED;
	}

	public BpAuthenticationException(Reason reason) {
		super(""); // Message isn't important, because user will see the reason
					// of exception according to locale.
		this.reason = reason;
	}
	
	private Reason reason;
	
	public enum Reason{
		LOGIN_MUST_BE_SET,
		PASSWORD_MUST_BE_SET,
		INVALID_LOGIN,
		INVALID_PASSWORD,
		INCORRECT_LOGIN,
		INCORRECT_PASSWORD, 
		UNRESOLVED
	}
	
}
