package com.bplatform.shared.constants;

public class PlatformServerConstants {

	public static final class ATTACHINFO {
		public static final String FILENAME = "fileName";
		public static final String DESCRIPTION = "description";
		public static final String DATE = "attachDate";
		public static final String UPLOAD = "upload";
		public static final String DOCUMENT_ID = "documentId";
		public static final String FILETYPE = "fileType";
	}
	public static class EXCEPTION {
		public final static String FILE_IS_TO_LARGE = "FILE_IS_TO_LARGE";
	}
	
}
