package com.bplatform.server.features.workflow.checkers;

import javax.enterprise.inject.Model;
import javax.inject.Inject;

import org.hibernate.Session;

import com.bplatform.server.MainFacade;
import com.bplatform.server.DAO.models.BpStatefulEntity;
import com.bplatform.server.DAO.models.MockDocument;
import com.bplatform.server.features.workflow.StateChangeCondition;

public class BpWfNode1toNode2Checker implements StateChangeCondition {
	
	@Inject
	MainFacade appFacade;

	@Override
	public <E extends BpStatefulEntity> boolean allowedToBe(E thisEntity,
			Session session) throws Exception {
		MockDocument md = (MockDocument) thisEntity;
		if (md.getSomeTestField()!=null) {
			return true;
		} else {
			return false;
		}
	}

}
