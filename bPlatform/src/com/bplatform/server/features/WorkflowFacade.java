package com.bplatform.server.features;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.hibernate.Session;

import com.bplatform.server.MainFacade;
import com.bplatform.server.DAO.APIs.BpWorkflowAPI;
import com.bplatform.server.DAO.models.BpStatefulEntity;
import com.bplatform.server.DAO.models.BpWorkflowNode;
import com.bplatform.server.DAO.models.BpWorkflowScheme;
import com.bplatform.server.features.workflow.OnStateChangeMutator;
import com.bplatform.server.features.workflow.StateChangeCondition;
import com.bplatform.shared.exceptions.BpProcessExecutionException;
import com.bplatform.shared.exceptions.DataBaseException;

@ApplicationScoped
public class WorkflowFacade {
	
	@Inject
	private MainFacade appFacade;
	
	/**
	 * Private option sets up from system properties facade on platform startup.
	 * <b>showStateCheckErrorNodes</b> is TRUE by default.
	 * <p>
	 * TRUE means that no error will be thrown on state change checkup method execution, exception will be logged and the mischecked node will be replaced by error-flag state node BpErrorCheckedNode containing information about error for client side.
	 * <p>
	 * FALSE means that if exception will be raised in check method it will be logged only and tha state node will be treated as not available for proceeding state change.
	 */
	private boolean showStateCheckErrorNodes = true;
	
	/**
	 * Private option sets up from system properties facade on platform startup.
	 * <b>throwStateCheckErrorException</b> is FALSE by default.
	 * <p>
	 * TRUE means that error will be thrown if some of state check methods will raise an error, so it can be shown as general error at the client side.
	 * So, no nodes at all will be returned by <b>getNextNodes</b> method, but the error <b>BpProcessExecutionException</b> will contain an information about the node (contained check method) and id of entity that caused it.
	 * This option overweights <b>showStateCheckErrorNodes</b>! 
	 */
	private boolean throwStateCheckErrorException = false;	
	
	@Inject //When the (stereotype) annotation @Model is declared on a class, it creates a request-scoped and named bean.
	private BpWorkflowAPI wfAPI;
	
	private HashMap<BpWorkflowScheme,HashSet<BpWorkflowNode>> schemes = null;
	private HashMap<String,BpWorkflowNode> nodes = null;
	private HashMap<String,StateChangeCondition> nodesCheckMth = null;
	private HashMap<String,OnStateChangeMutator> nodesOnChangeMth = null;
	private boolean errFlag = false;
	
	
	/**
	 * <b>IMPORTANT!!!</b>
	 * <p>
	 * Must be checked BEFORE using resettled workflow. If <b>true</b> then some methods in nodes was not properly saved from DB.
	 * <p>
	 * P.S. I really don't know how to do it in more Java way except of setting in node some predefined method that will throw an error when used.
	 * maybe it'll  be more suitable.  
	 * @return
	 */
	public boolean hasErrors() {
		return errFlag;
	}
	
	public BpWorkflowAPI getWfAPI() {
		return wfAPI;
	}

	/**
	 * Block entity, check user rights, check conditions, run on change mutator, switch state, unblock entity.
	 * TODO suppress checking on forced state change (??adm rights)
	 * @param nextNode
	 * @param currEntity
	 * @param forced
	 * @return
	 * @throws BpProcessExecutionException
	 */
	public <E extends BpStatefulEntity> E changeState(BpWorkflowNode nextNode, E currEntity, boolean forced) throws BpProcessExecutionException, DataBaseException {
		Session sess = appFacade.getHibernateFacade().getHibernateSession();
		//block entity
		try {
			wfAPI.blockEntity((BpStatefulEntity) currEntity, sess);
		} catch (BpProcessExecutionException e) {
			throw e;
		} catch (DataBaseException e) {
			throw e;
		}
		boolean allow = false;
		BpProcessExecutionException bpe = null;
		//try to check state
		try {
			allow = runCheckMethod(nextNode, currEntity, sess);
		} catch (BpProcessExecutionException e) {
			bpe = e;
		}
		//if we have no errors and got allow==true
		if (allow) {
			try {
				currEntity = runOnChangeMutator(nextNode, currEntity, sess);
			} catch (Exception e) {
				bpe = new BpProcessExecutionException(currEntity.getId(),nextNode.getStatusName());
			}
			//if everything is still fine - change state and commit transaction, else - rollback anything done before
			if (bpe!=null) {
				try {
					currEntity = wfAPI.setState(nextNode, currEntity, sess);
				} catch (Exception e) {
				}
			} else {
				sess.getTransaction().rollback();
			}
		}
		try {
			wfAPI.unblockEntity((BpStatefulEntity) currEntity, sess);
		} catch (BpProcessExecutionException e) {
			throw e;
		} catch (DataBaseException e) {
			throw e;
		}
		sess.close();
		return currEntity;
	}
	
	/**
	 * Returns set of nodes succeeding the given one.
	 * Only allowed for current user and for entities that satisfying conditions to change. 
	 * @param entity
	 * @return
	 * @throws Exception if something went wrong while checking
	 */
	public <E extends BpStatefulEntity> Set<BpWorkflowNode> getNextNodes(E entity) throws BpProcessExecutionException {
		BpWorkflowNode tmp = nodes.get(entity.getState().getId());
		Set<BpWorkflowNode> nextNodes = tmp.getSuccessors();
		Set<BpWorkflowNode> allowedNodes = new HashSet<>();
		Session sess = appFacade.getHibernateFacade().getHibernateSession();
		for (BpWorkflowNode n:nextNodes) {
			//TODO add user rights check
			try {
				if (runCheckMethod(n,entity,sess)==true) {
					allowedNodes.add(n);
				}
			} catch (BpProcessExecutionException e) {
				if (throwStateCheckErrorException) {
					throw e;
				}
				if (showStateCheckErrorNodes) {
					//TODO make class for MockErrorNode
				}
			}
		}
		return allowedNodes;
	}
	
	/**
	 * Returns true if check method for given next node allows to change entity state to it.
	 * If no check class specified - returns true  
	 * @param node
	 * @return
	 */
	private <E extends BpStatefulEntity> boolean runCheckMethod(BpWorkflowNode nextNode, E currEntity, Session session) throws BpProcessExecutionException {
		if (nextNode.getConditionsCheckClass()!=null&&(!nextNode.getConditionsCheckClass().equals(""))) {
			try {
				StateChangeCondition cc = nodesCheckMth.get(nextNode.getId());
				return cc.allowedToBe(currEntity, session);
			} catch (Exception e) {
				e.printStackTrace();
				BpProcessExecutionException ex = new BpProcessExecutionException(currEntity.getId(),nextNode.getStatusName());
				throw ex;
			}
		} else {
			return true;
		}
	}
	
	/**
	 * Runs mutator method if it is present in target state node. Only after it successful execution an entity will change state.  
	 * Can throw an exception, if so - no 
	 * @param nextNode
	 * @param currEntity
	 * @throws Exception
	 */
	private <E extends BpStatefulEntity> E runOnChangeMutator(BpWorkflowNode nextNode, E currEntity, Session session) throws Exception {
		try {
			OnStateChangeMutator cc = nodesOnChangeMth.get(nextNode.getId());
			return cc.onChange(currEntity, session);
		} catch (Exception e) {
			e.printStackTrace();
			BpProcessExecutionException ex = new BpProcessExecutionException(currEntity.getId(),nextNode.getStatusName());
			throw ex;
		}
	}
	
	/**
	 * Because of using Hibernate option that limits joined sets entities in containing only an ID - we must persist entities before using them.
	 * Persist means not the DB persistence, but persistence with cached <i>nodes</i> that themself must be maintained persist with DB to minimize querying.
	 * NOT NEEDED???
	 * @param nodes
	 */
	private Set<BpWorkflowNode> persistWithCached(Set<BpWorkflowNode> nodes) {
		for (BpWorkflowNode node:nodes) {
			node = this.nodes.get(node.getId());
		}
		return nodes;
	}
	
	/**
	 * Returns a map-type container of scheme-nodes set stored as a class variable.
	 * Pass "true" to reload from DB (also fills class variable HashMap<String,BpWorkflowNode>)   
	 * @param reload
	 * @return HashMap<BpWorkflowScheme,HashSet<BpWorkflowNode>>
	 */
	public HashMap<BpWorkflowScheme,HashSet<BpWorkflowNode>> getSchemes(Boolean reload) {
		
		//we should wait while schemes are updated to maintain consistency
		try {
			while (needToWait) {
				Thread.sleep(1000);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if (schemes==null||(reload!=null&&reload==true)) {
			schemes = reloadSchemes();
		}
		return schemes;		
	}
	
	/**
	 * Stop flag for shchemesUpdate proc.
	 */
	private boolean needToWait = false;
	
	@PostConstruct
	private synchronized HashMap<BpWorkflowScheme,HashSet<BpWorkflowNode>> reloadSchemes() {
		needToWait = true;
		errFlag = false;
		schemes = new HashMap<BpWorkflowScheme,HashSet<BpWorkflowNode>>();
		nodes = new HashMap<String,BpWorkflowNode>();
		nodesCheckMth = new HashMap<String, StateChangeCondition>();
		nodesOnChangeMth = new HashMap<String, OnStateChangeMutator>();
		Set<BpWorkflowScheme> tmpSchemes = wfAPI.getSchemes();
		for (BpWorkflowScheme sch:tmpSchemes) {
			Set<BpWorkflowNode> tmpNodes = wfAPI.getWorkflowNodesSet(sch);
			if (tmpNodes!=null) {
				schemes.put(sch, (HashSet<BpWorkflowNode>) tmpNodes);
				for (BpWorkflowNode node:tmpNodes) {
					nodes.put(node.getId(), node);
					if (node.getConditionsCheckClass()!=null) {
						try {
							Class controllerClass = Class.forName(node.getConditionsCheckClass());
							StateChangeCondition controller = (StateChangeCondition) controllerClass.newInstance();
							nodesCheckMth.put(node.getId(), controller);
						} catch (Exception e) {
							e.printStackTrace();
							errFlag = true;
						}
					}
					if (node.getOnChangeClass()!=null) {
						try {
							Class controllerClass = Class.forName(node.getOnChangeClass());
							OnStateChangeMutator controller = (OnStateChangeMutator) controllerClass.newInstance();
							nodesOnChangeMth.put(node.getId(), controller);
						} catch (Exception e) {
							e.printStackTrace();
							errFlag = true;
						}
					}
				}
			}
		}
		needToWait = false;
		return schemes;
	}
}
