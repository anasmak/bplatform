package com.bplatform.server.services;

import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;

import com.bplatform.client.bpShared.DTO.BpGroupDTO;
import com.bplatform.client.mainModule.services.GroupAdminService;
import com.bplatform.server.MainFacade;
import com.bplatform.server.DAO.HibernateFacade;
import com.bplatform.server.DAO.models.BpUserGroup;
import com.bplatform.shared.exceptions.DuplicateNameException;
import com.bplatform.shared.exceptions.NameValidationException;
import com.bplatform.shared.validators.FieldVerifier;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * 
 * RPC Implementation Own User Groups
 * 
 * @author IhorG
 */
@WebServlet
public class GroupAdminServiceImpl extends RemoteServiceServlet implements
		GroupAdminService {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Inject
	private MainFacade appFacade;

	@Inject
	private HibernateFacade hibFacade;

	/**
	 * Get Group Properties
	 */
	@Override
	public BpGroupDTO getGroupProperties(String groupId) {
		BpGroupDTO bpGroupDTO = new BpGroupDTO();
		if (groupId != null) {
			BpUserGroup bpUserGroup = hibFacade.getBpUserGroupAPI().getById(groupId);
			
			if (bpUserGroup == null) {
				throw new NullPointerException("Group with ID:" + groupId + ", don't exist!");
			} else {
				bpGroupDTO.setGroupId(bpUserGroup.getId());
				bpGroupDTO.setName(bpUserGroup.getName());
				bpGroupDTO.setParentId(bpUserGroup.getParentId());
			}
			
		} else {
			throw new NullPointerException("Group ID is NULL!");
		}

		return bpGroupDTO;
	}

	/**
	 * ADD or Update Group
	 */
	@Override
	public String addOrUpdateGroup (BpGroupDTO bpGroupDTO) throws Exception {

		String resultOfTransaction = null;

		try {
						
			if (FieldVerifier.isValidName(bpGroupDTO.getName())) {
				
				BpUserGroup bpUG = new BpUserGroup(bpGroupDTO.getName(), bpGroupDTO.getParentId());
//				bpUG.setName(bpGroupDTO.getName());
//				bpUG.setParentID(bpGroupDTO.getParentId());
//				bpUG.setId(bpGroupDTO.getGroupId());
				System.out.println("Parent group is: "
						+ bpGroupDTO.getParentId() + " Name is: "
						+ bpGroupDTO.getName());
				
				if (bpUG != null) {
					
					BpUserGroup tempBpGroupName = hibFacade.getBpUserGroupAPI().getGroupByName(bpGroupDTO.getName());
					if (tempBpGroupName != null && tempBpGroupName.getId() != bpGroupDTO.getGroupId()) {
					
						throw new DuplicateNameException();

					}
					
					hibFacade.getBpUserGroupAPI().saveOrUpdate(bpUG);
					resultOfTransaction = "Group was successfully added!";
				}
				
			} else {
				
				throw new NameValidationException("Name is: "
						+ bpGroupDTO.getName() + ". Parent group is: "
						+ bpGroupDTO.getParentId());
			
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultOfTransaction = "Group was not added!";
			throw e;
		}

		return resultOfTransaction;
	}


	/**
	 * DELETE group
	 */
	@Override
	public String deleteGroup(String groupId) throws Exception {
		BpUserGroup bpUserGroup = null;
		String resultOfTransaction = null;
		if (groupId != null) {
			List<BpUserGroup> groupChildList = hibFacade.getBpUserGroupAPI().getChildrenGroups(groupId);
			
			if (groupChildList.isEmpty()) {
				Set<String> usersSetFromGroup = hibFacade.getBpUserGroupAPI().getByIdUserList(groupId);
				
				if (usersSetFromGroup.isEmpty()) {
					bpUserGroup = hibFacade.getBpUserGroupAPI().getById(groupId);
						if (bpUserGroup.getParentId() != null) {
							hibFacade.getBpUserGroupAPI().delete(bpUserGroup);
							resultOfTransaction = "Group was delete successfully!";
						} else {
							resultOfTransaction = "You can't delete Root Category!";
						}
				} else {
					resultOfTransaction = "Group contains next user(s): " + usersSetFromGroup.toString() +".\nPlease, unassign them previously";
				}
				
			} else {
				resultOfTransaction = "Group contains children groups! Please, delete them previously";
			}
			
		} else {
			resultOfTransaction = "Group ID is NULL!";
		}

		return resultOfTransaction;
	}

}
