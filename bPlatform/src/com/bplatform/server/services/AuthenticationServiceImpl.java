package com.bplatform.server.services;

import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.PasswordEncoder;

import com.bplatform.client.authModule.services.AuthService;
import com.bplatform.server.MainFacade;
import com.bplatform.server.DAO.APIs.BpUserAPI;
import com.bplatform.server.DAO.models.BpUser;
import com.bplatform.shared.exceptions.BpAuthenticationException;
import com.bplatform.shared.exceptions.BpAuthenticationException.Reason;
import com.bplatform.shared.validators.FieldVerifier;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server side implementation of the RPC service.
 * 
 * @author Anastasia Smakovska
 */
@WebServlet
public class AuthenticationServiceImpl extends RemoteServiceServlet implements AuthService {
	
	private static final long serialVersionUID = 3910229364196291882L;
	
	@Inject
	private MainFacade appFacade;

	@Override
	public String userAuth(String login, String password) throws BpAuthenticationException {
		//TODO if authorized set user in session context, return true
		//TODO else return false
		//MOCK here
		
		//login or password can't be null according to existing models
		if(login == null){
			throw new BpAuthenticationException(Reason.LOGIN_MUST_BE_SET);
		}
		if(password == null){
			throw new BpAuthenticationException(Reason.PASSWORD_MUST_BE_SET);
		}
		
		login = escapeHtml(login);
		password = escapeHtml(password);
		if(!FieldVerifier.isValidLogin(login)){
			throw new BpAuthenticationException(Reason.INVALID_LOGIN);
		}
		if(!FieldVerifier.isValidPassword(password)){
			throw new BpAuthenticationException(Reason.INVALID_PASSWORD);
		}
		
		BpUserAPI api = appFacade.getHibernateFacade().getBpUserAPI();
		BpUser bpUser = api.getByLogin(login); 
		if(bpUser == null){
			throw new BpAuthenticationException(Reason.INCORRECT_LOGIN);
		}
	
		PasswordEncoder encoder = new Md5PasswordEncoder();
		if(!encoder.isPasswordValid(bpUser.getPassword(), password, bpUser.getEmail())){
			throw new BpAuthenticationException(Reason.INCORRECT_PASSWORD);
		}
		
		appFacade.getSessionContext().setSessionUserModel(bpUser);	
		//TODO return string from parametrs + /restricted/BPlatform.html
		return "http://127.0.0.1:8888/restricted/BPlatform.html?gwt.codesvr=127.0.0.1:9997";
	}

	/**
	 * Escape an html string. Escaping data received from the client helps to
	 * prevent cross-site script vulnerabilities.
	 * 
	 * @param html the html string to escape
	 * @return the escaped string
	 */
	private String escapeHtml(String html) {
		if (html == null) {
			return null;
		}
		return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;");
	}
	
}
