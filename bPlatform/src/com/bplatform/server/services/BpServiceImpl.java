package com.bplatform.server.services;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.inject.Inject;

import com.bplatform.client.mainModule.services.BPService;
import com.bplatform.server.MainFacade;
import com.bplatform.server.annotations.BPMock;
import com.bplatform.server.listeners.SessionListener;
import com.bplatform.shared.exceptions.NoCustomPropertiesException;
import com.bplatform.shared.exceptions.NoLocalPropException;
import com.bplatform.shared.exceptions.NoSetNewPropExeption;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server side implementation of the RPC service.
 */
public class BpServiceImpl extends RemoteServiceServlet implements BPService {

	private static final long serialVersionUID = 7899318500242246993L;

	@Inject
	private MainFacade appFacade;

	@BPMock
	@Override
	public String startSrv(String input) throws IllegalArgumentException {
		return "Hello" + SessionListener.getAppContextUserStorage().size();
	}

	/**
	 * 
	 * returns a local HashMap<String, String> from locales.properties for
	 * "local"_"local resourse"
	 * 
	 * ?????????? ????????  ?????? ???  ????????? ?????? ????? (local) ? ????????????????? (localResourse). 
	 * 
	 * BP_??????????_???????????????.property
	 * 
	 */
	@Override
	public HashMap<String, String> getLocalPropMap(String local, String localResourse) throws NoLocalPropException, Exception {

		HashMap<String, String> tempMap = new HashMap<String, String>();

		try {
			ResourceBundle resource = appFacade.getPropertiesFacade().getLocalePropMapForLocAndResours(local, localResourse);

			Enumeration bundleKeys = resource.getKeys();
			while (bundleKeys.hasMoreElements()) {
				String key = (String) bundleKeys.nextElement();
				String value = resource.getString(key);
				tempMap.put(key, value);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return tempMap;

	}

	/**
	 * ?????????? ????? ???? ????????? ??????? ? ???? ????????? ??????????????? 
	 * (????? ?????????)
	 * 
	 * @see
	 * com.bplatform.client.mainModule.services.BPService#getAllLocalPropMap()
	 */
	@Override
	public Map<String, Map<String, Map<String, String>>> getAllLocalPropMap() throws NoLocalPropException, Exception {

		Map<String, Map<String, Map<String, String>>> locMap = new HashMap<String, Map<String, Map<String, String>>>();

		try {
			HashMap<String, HashMap<String, ResourceBundle>> localMap = appFacade.getPropertiesFacade().getLocaleMap();

			Set<String> locSet = localMap.keySet();

			for (String locKey : locSet) {
				HashMap<String, ResourceBundle> resLocMap = localMap.get(locKey);
				Set<String> resSet = resLocMap.keySet();
				Map<String, Map<String, String>> localesResTemp = new HashMap<String, Map<String, String>>();
				for (String resKey : resSet) {
					ResourceBundle resource = resLocMap.get(resKey);

					Enumeration bundleKeys = resource.getKeys();
					HashMap<String, String> tempMap = new HashMap<String, String>();
					while (bundleKeys.hasMoreElements()) {
						String key = (String) bundleKeys.nextElement();
						String value = resource.getString(key);
						tempMap.put(key, value);
					}
					localesResTemp.put(resKey, tempMap);
				}
				locMap.put(locKey, localesResTemp);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return locMap;

	}

	/**
	 * 
	 * returns a custom local( for custom prop name "customPropName.properties")
	 * ??????????  ????????  ????????? ????????  ??? ????? customPropName = ????????.properties
	 * (????????? ???????? ??? ??????? ?? ?????????? ? BP_)
	 */
	@Override
	public HashMap<String, String> getCustomProp(String customPropName) throws NoCustomPropertiesException, Exception {
		ResourceBundle resource = appFacade.getPropertiesFacade().getCustomProp(customPropName);

		HashMap<String, String> tempMap = new HashMap<String, String>();

		Enumeration bundleKeys = resource.getKeys();
		while (bundleKeys.hasMoreElements()) {
			String key = (String) bundleKeys.nextElement();
			String value = resource.getString(key);
			tempMap.put(key, value);
		}
		return tempMap;
	}

	
	/**
	 * Save new properties
	 */
	/**
	 * ?????????????(??????????????) ???? ???????? ( fileName ????????.properties) ?????? ?????????? 
	 */
	@Override
	public boolean  setNewProp(String fileName, HashMap<String,String> res ) throws NoSetNewPropExeption {
		
		 throw new  NoSetNewPropExeption() ;
		// appFacade.getPropertiesFacade().saveToFile(fileName, res);
						
	//	return true;
		
	}
	
	
	
	
}
