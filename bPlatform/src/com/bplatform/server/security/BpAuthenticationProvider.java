package com.bplatform.server.security;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import com.bplatform.server.MainFacade;
import com.bplatform.server.DAO.APIs.BpUserAPI;
import com.bplatform.server.DAO.models.BpPermission;
import com.bplatform.server.DAO.models.BpUser;
import com.bplatform.server.DAO.models.JoinedBpUserToBpPermission;
import com.bplatform.shared.exceptions.BpAuthenticationException;
import com.bplatform.shared.exceptions.BpAuthenticationException.Reason;
import com.bplatform.shared.validators.FieldVerifier;


public class BpAuthenticationProvider implements AuthenticationProvider {

	@Inject
	private MainFacade appFacade;
	
	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {
		String login = (String) authentication.getPrincipal();
		String password = (String) authentication.getCredentials();
		
		if(login == null){
			throw new BpAuthenticationException(Reason.LOGIN_MUST_BE_SET);
		}
		if(password == null){
			throw new BpAuthenticationException(Reason.PASSWORD_MUST_BE_SET);
		}
		
		login = escapeHtml(login);
		password = escapeHtml(password);
		if(!FieldVerifier.isValidLogin(login)){
			throw new BpAuthenticationException(Reason.INVALID_LOGIN);
		}
		if(!FieldVerifier.isValidPassword(password)){
			throw new BpAuthenticationException(Reason.INVALID_PASSWORD);
		}
		
		BpUserAPI api = appFacade.getHibernateFacade().getBpUserAPI();
		BpUser bpUser = api.getByLogin(login); 
		if(bpUser == null){
			throw new BpAuthenticationException(Reason.INCORRECT_LOGIN);
		}
	
		PasswordEncoder encoder = new Md5PasswordEncoder();
		if(!encoder.isPasswordValid(bpUser.getPassword(), password, bpUser.getEmail())){
			throw new BpAuthenticationException(Reason.INCORRECT_PASSWORD);
		}
		
		Set<JoinedBpUserToBpPermission> joinedPermissions = bpUser.getJoinedPermissions();
		HashSet<BpPermission> permissions = new HashSet<BpPermission>();
		for(JoinedBpUserToBpPermission joined : joinedPermissions){
			if(joined.isPermitted()){
				permissions.add(joined.getBpPermission());
			}
		}
		
		new BpAuthentication(permissions, authentication, "", true, true, login, password);
		appFacade.getSessionContext().setSessionUserModel(bpUser);	
		return null;
	}

	//TODO may be improvement will be required
	@Override
	public boolean supports(Class<?> authentication) {	
		return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
	}
	
	/**
	 * Escape an html string. Escaping data received from the client helps to
	 * prevent cross-site script vulnerabilities.
	 * 
	 * @param html the html string to escape
	 * @return the escaped string
	 */
	private String escapeHtml(String html) {
		if (html == null) {
			return null;
		}
		return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;");
	}
	

}
