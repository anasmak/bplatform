package com.bplatform.server.security;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import com.bplatform.server.DAO.HibernateFacade;
import com.bplatform.server.DAO.APIs.BpTokenAPI;
import com.bplatform.server.DAO.models.BpToken;
import com.bplatform.shared.exceptions.DataBaseException;
/**
 * 
 * 
 * @author Anastasia Smakovska
 *
 */
public class BpTokenRepository implements PersistentTokenRepository{
	
	@Inject HibernateFacade hibernateFacade;
	
	@Override
    public void createNewToken(PersistentRememberMeToken token) {
		try{
			hibernateFacade.getBpTokenAPI().save(new BpToken(token));
		} catch(ConstraintViolationException e){
			// There's no harm in, so system wouldn't be stopped, but token wasn't saved.
			System.err.println("Token of user " + token.getUsername() + " cannot be saved by the reason of ConstraintViolationException"); 
		} catch(DataBaseException e){
			// The same case as previous, there's no harm in
			System.err.println("Token of user " + token.getUsername() + " cannot be saved by the reason of DataBaseException");
		}
    }
 
	
    @Override
    public void updateToken(String series, String tokenValue, Date creationDate) {
    	try{
    		BpTokenAPI bpTokenAPI = hibernateFacade.getBpTokenAPI();
			List<BpToken> tokens = bpTokenAPI.getBySeries(series);
			//Remember that series is random number, so there's is tiny probability to be not unique
			//So we should use second verification of date
			if(tokens != null && !tokens.isEmpty()){
				BpToken token = tokens.remove(0);
				while(!token.getDate().equals(creationDate)){
					if(!tokens.isEmpty()){
						System.err.println("Token with series and date wasn't found.");
						return;
					}
					token = tokens.remove(0);
				}
				token.setTokenValue(tokenValue);
				bpTokenAPI.update(token);
			}
		}  catch(ConstraintViolationException e){
			// There's no harm in, so system wouldn't be stopped, but token wasn't updated.
			System.err.println("Token with series " + series + " cannot be updated by the reason of ConstraintViolationException"); 
		}catch(DataBaseException e){
			// The same case as previous, there's no harm in
			System.err.println("Token with series " + series + " cannot be updated by the reason of DataBaseException");
		} 
    }
 
    @Override
    public PersistentRememberMeToken getTokenForSeries(String series) {
    	try{
    		//TODO care about not unique series number or match as deprecated
    		List<BpToken> tokens = hibernateFacade.getBpTokenAPI().getBySeries(series);
    		if(tokens != null && !tokens.isEmpty()){
    			BpToken token = tokens.remove(0);
    			return new PersistentRememberMeToken(token.getUsername(), token.getSeries(), token.getTokenValue(), token.getDate());
    		}
    	}catch(ConstraintViolationException e){
    		// There's no harm in, so system wouldn't be stopped, but token wasn't received.
    		System.err.println("PersistentRememberMeToken with series " + series + " cannot be recieved by the reason of ConstraintViolationException"); 
    	} catch(DataBaseException e){
			// The same case as previous, there's no harm in
			System.err.println("PersistentRememberMeToken with series " + series + " cannot be recieved by the reason of DataBaseException");
		} 
    	return null;
    }
 
    @Override
    public void removeUserTokens(String username) {
    	try{
    		BpTokenAPI bpTokenAPI = hibernateFacade.getBpTokenAPI();
    		List<BpToken> tokens = bpTokenAPI.getByUsername(username);
    		if(tokens != null){
    			for(BpToken token : tokens){
    				bpTokenAPI.delete(token);
    			}
    		}
    	}catch(ConstraintViolationException e){
    		// There's no harm in, so system wouldn't be stopped, but tokens weren't deleted.
    		System.err.println("Tokens with username " + username + " cannot be deleted by the reason of ConstraintViolationException"); 
    	} catch(DataBaseException e){
			// The same case as previous, there's no harm in
			System.err.println("Tokens with username " + username + " cannot be deketed by the reason of DataBaseException");
		} 
    	List<BpToken> tokens = hibernateFacade.getBpTokenAPI().getByUsername(username);
    	if(tokens != null && !tokens.isEmpty()){
    		
    	}
    }

}
