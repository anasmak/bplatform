package com.bplatform.server.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


//TODO �������� � ���������� ���
public class DownloadFileServlet extends HttpServlet {

	private static final long serialVersionUID = -5047243767902231822L;

	protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
		try {

			String fileName = "test.txt";
			String mimeType = "application/download-binary";

			response.setContentType("text/html");

			response.setHeader("Content-disposition", fileName);
           // ��������� ����
			String path = "C:" + File.separator + "Users" + File.separator + "volovoda" + File.separator + "Sreda" + File.separator + "testUpload" + File.separator;
			String storageFileName = path + fileName;

			File file = new File(storageFileName);

			OutputStream out = response.getOutputStream();
			FileInputStream in = new FileInputStream(file);
			byte[] buffer = new byte[4096];
			int length;
			while ((length = in.read(buffer)) > 0) {
				out.write(buffer, 0, length);
			}
			in.close();
			out.flush();

		} catch (Exception e) {
			throw new ServletException(e);
		}
	}
}
