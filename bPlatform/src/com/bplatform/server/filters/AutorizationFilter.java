package com.bplatform.server.filters;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bplatform.server.MainFacade;
import com.bplatform.server.features.DebugFeatures;

/**
 * Filters all requests - sessions without user logged are knocked back to login page.
 * Also, for debug, runs a utility method and redirects instantly to mainModule with "admin" logged in session.  
 * @author Smak
 */
@WebFilter
public class AutorizationFilter implements Filter {
	
	protected FilterConfig filterConfig;
	
	@Inject
	private MainFacade appFacade;
	
	@Inject
	DebugFeatures dbg;
	
	
	@Override
	public void destroy() {
		filterConfig = null;		
	}
	
	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		final HttpServletRequest request = (HttpServletRequest) servletRequest;
		final HttpServletResponse response = (HttpServletResponse) servletResponse;
		
		HttpSession session = ((HttpServletRequest) request).getSession(false);
		if (session != null) {
			Object userDTO = new Object();
			//EntityDTO userDTO = SSOHelper.getCurrentUser(request, filterConfig.getServletContext());
			userDTO = appFacade.getSessionContext().getSessionUser();			
			//userDTO = sCont.getSessionUser();
			if (userDTO == null) {
				response.sendRedirect(request.getContextPath() + filterConfig.getInitParameter("error_page"));
			} else {
				filterChain.doFilter(request, response);
			}
		} else {
			if (appFacade.getAppProperties().isDebugMode()) {
				
				dbg.doPrepareDebug();
								
				response.sendRedirect("http://127.0.0.1:8888/restricted/BPlatform.html?gwt.codesvr=127.0.0.1:9997");
			} else {
				response.sendRedirect(request.getContextPath() + filterConfig.getInitParameter("error_page"));
			}
		}
		
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;		
	}

}
