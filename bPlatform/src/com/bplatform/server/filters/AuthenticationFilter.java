package com.bplatform.server.filters;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bplatform.server.MainFacade;
import com.bplatform.server.features.DebugFeatures;

@WebFilter
public class AuthenticationFilter implements Filter{

	protected FilterConfig filterConfig;
	
	@Inject
	private MainFacade appFacade;
	
	@Inject
	DebugFeatures dbg;
	
	@Override
	public void destroy() {
		filterConfig = null;
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
		FilterChain filterChain) throws IOException, ServletException {
		
		final HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
		final HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
		
		HttpSession session = httpServletRequest.getSession(false);
		
		if (session != null) {
			Object userDTO = new Object();
			userDTO = appFacade.getSessionContext().getSessionUser();			
			if (userDTO == null) {
				httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + filterConfig.getInitParameter("error_page"));
			} else {
				filterChain.doFilter(httpServletRequest, httpServletResponse);
			}
		} else {
			if (appFacade.getAppProperties().isDebugMode()) {
				
				//dbg.doPrepareDebug();
								
				httpServletResponse.sendRedirect("http://127.0.0.1:8888/restricted/BPlatform.html?gwt.codesvr=127.0.0.1:9997");
			} else {
				httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + filterConfig.getInitParameter("error_page"));
			}
		}
		
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;		
	}

}
