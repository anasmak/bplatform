package com.bplatform.server.DAO;

import java.io.Serializable;
import java.util.UUID;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import org.hibernate.Session;

import com.bplatform.server.MainFacade;

@SessionScoped
public class HibernateSession implements Serializable {
	
	private static final long serialVersionUID = -2400779598239748051L;

	private String hSessionId;
	private Session hSession;
	
	@Inject
	private MainFacade appFacade;	
	
//	@Inject
//	private HibernateFacade hFacade;
	
	public String getHibernateSessionId() {
		return hSessionId;
	}
	
	public HibernateSession() {
		hSessionId = UUID.randomUUID().toString();
//		this.hSession = hFacade.getHibernateSession();
		appFacade.getSessionContext().addHibernateSession(this);
	}

}
