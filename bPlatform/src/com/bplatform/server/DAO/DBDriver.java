package com.bplatform.server.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.bplatform.server.annotations.BPMock;

/**
 *
 * @author Smak
 */
@BPMock
public class DBDriver {

	//comment
    public static Connection getConnection() throws SQLException {
        
    	//TODO @Min: use ResourceBundle in ApplicationScope for cached parameters store 
        ResourceBundle resource = ResourceBundle.getBundle("config/db");
        String url = resource.getString("url");
        String driver = resource.getString("driver");
        String user = resource.getString("user");
        String pass = resource.getString("password");
        
        try {
            Class.forName(driver).newInstance();
        } catch (ClassNotFoundException e) {
            throw new SQLException("Driver wasn't added!\n" + e.getCause());
        } catch (InstantiationException e) {
            throw new SQLException(e.getCause());
        } catch (IllegalAccessException e) {
            throw new SQLException(e.getCause());
        }
        
        return DriverManager.getConnection(url, user, pass);
        
    }
}
