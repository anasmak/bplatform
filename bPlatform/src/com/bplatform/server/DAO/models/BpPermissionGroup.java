package com.bplatform.server.DAO.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Class for storing information about groups which aggregate security
 * permissions.
 * 
 * @author Anastasia Smakovska
 * 
 */
@Entity
@Table(name = "BP_PERMISSION_GROUP", uniqueConstraints = { @UniqueConstraint(columnNames = { "name" }) })
public class BpPermissionGroup extends BpTree {

	@Column(length = 32)
	private String code;

	// Annotations for generating new table for storing BpPermissionGroup to
	// BpPermission relation in the database
	//
	// Note: you should use full path of targetEntity class, because otherwise
	// Hibernate 4.2 can't determine class path and doesn't throw an exception
	// in this case. It looks ugly, but don't remove class path!
	// Remember: mappedBy and targetEntity is used for avoiding creation excess
	// tables by Hibernate. Instead of this only special columns for id are
	// added to tables.
	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
	@JoinTable(name = "BP_PERMISSIONS_TO_BP_PERMISSION_GROUPS", joinColumns = @JoinColumn(name = "group_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "permission_id", referencedColumnName = "id"))
	private Set<com.bplatform.server.DAO.models.BpPermission> permissions = new HashSet<com.bplatform.server.DAO.models.BpPermission>();

	// Annotations for generating new table for storing BpPermissionGroup to
	// BpUserGroup relation in the database
	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "permissionGroups", targetEntity = com.bplatform.server.DAO.models.BpUserGroup.class)
	private Set<com.bplatform.server.DAO.models.BpUserGroup> bpUserGroups = new HashSet<com.bplatform.server.DAO.models.BpUserGroup>();

	/**
	 * If someone creates new instance without specifying needed fields - it must be his/her own error, we must not allow to save such a retarded entity to DB!  
	 */
	BpPermissionGroup() {}

	public BpPermissionGroup(String name) {
		super(name);
		this.code = name;
	}

	public BpPermissionGroup(String ID, String name, String parentID,
			String tCode, String connectedTo, String permCode) {
		super(ID, name, parentID, tCode, connectedTo);
		this.code = permCode;
	}

	public BpPermissionGroup(String ID, String name, String parentID,
			String tCode, String connectedTo) {
		super(ID, name, parentID, tCode, connectedTo);
	}

	public void setPermCode(String permCode) {
		this.code = permCode;
	}

	public void setPermissions(
			Set<com.bplatform.server.DAO.models.BpPermission> permissions) {
		if (permissions == null) {
			this.permissions = new HashSet<com.bplatform.server.DAO.models.BpPermission>();
		} else {
			this.permissions = permissions;
		}
	}

	public Set<com.bplatform.server.DAO.models.BpPermission> getPermissions() {
		return permissions;
	}

	public String getCode() {
		return code;
	}

	public Set<com.bplatform.server.DAO.models.BpUserGroup> getBpUserGroups() {
		return bpUserGroups;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setBpUserGroups(
			Set<com.bplatform.server.DAO.models.BpUserGroup> bpUserGroups) {
		if (bpUserGroups == null) {
			this.bpUserGroups = new HashSet<com.bplatform.server.DAO.models.BpUserGroup>();
		} else {
			this.bpUserGroups = bpUserGroups;
		}

	}

	public boolean addBpPermission(BpPermission permission) {
		return permissions.add(permission);
	}

	public boolean addBpUserGroup(BpUserGroup userGroup) {
		return bpUserGroups.add(userGroup);
	}

}
