package com.bplatform.server.DAO.models;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.GenericGenerator;

@MappedSuperclass
public class BpDefaultEntity {
	
	@Id
	@GeneratedValue(generator="uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	protected String id;
	
	
	public BpDefaultEntity() {}
	
    public String getId() {
		return id;
    }

    private void setId(String id) {
		this.id = id;
    }
	
}
