package com.bplatform.server.DAO.models;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.sun.istack.internal.NotNull;

/**
 * Workflow nodes. 
 * <p>
 * Not inherited from BP_TREE because the MTM type of connection. 
 * No need to programmatically extend the API by using bp_tree join mechanism.
 * @author Minolan
 */
@NamedQueries({
		@NamedQuery(name = "getNodesOfWorkflow", query = "FROM BpWorkflowNode node WHERE node.scheme = :scheme AND node.enabled=1"),
		@NamedQuery(name = "getNotEnabledNodes", query = "FROM BpWorkflowNode node WHERE node.enabled=0")
	})
@Entity
@Table(name = "BP_WORKFLOW_NODES")
public class BpWorkflowNode extends BpDefaultEntity {
	
	/**
	 * System name (may be not unique in different workflow scopes)
	 */
	@Column(nullable = false)
	protected String statusName;
	
	/**
	 * Set to true if node is an entry point (default value for newly created stateful entity).
	 */
	protected Boolean entryNode;
	
	protected String conditionsCheckClass;
	
	protected String onChangeClass;
	
	/**
	 * Default value == true
	 */
	protected Boolean enabled = true;
	
	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch=FetchType.EAGER)
	@JoinTable(name = "BP_WORKFLOWNODE_FROM_TO", joinColumns = @JoinColumn(name = "from_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "to_id", referencedColumnName = "id"))
	private Set<com.bplatform.server.DAO.models.BpWorkflowNode> successors = new HashSet<com.bplatform.server.DAO.models.BpWorkflowNode>();
		
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })//optional=false
	@JoinColumn(name = "permission_id")
	private com.bplatform.server.DAO.models.BpPermission allowedFor = new com.bplatform.server.DAO.models.BpPermission();
	
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, optional=false)
	@JoinColumn(name = "scheme_id")
	private com.bplatform.server.DAO.models.BpWorkflowScheme scheme = new com.bplatform.server.DAO.models.BpWorkflowScheme();
	

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public Boolean getEntryNode() {
		return entryNode;
	}

	public void setEntryNode(Boolean entryNode) {
		this.entryNode = entryNode;
	}

	public Set<com.bplatform.server.DAO.models.BpWorkflowNode> getSuccessors() {
		return successors;
	}

	public void setSuccessors(
			Set<com.bplatform.server.DAO.models.BpWorkflowNode> successors) {
		this.successors = successors;
	}

	public com.bplatform.server.DAO.models.BpPermission getAllowedFor() {
		return allowedFor;
	}

	public void setAllowedFor(com.bplatform.server.DAO.models.BpPermission allowedFor) {
		this.allowedFor = allowedFor;
	}

	public com.bplatform.server.DAO.models.BpWorkflowScheme getScheme() {
		return scheme;
	}

	public void setScheme(com.bplatform.server.DAO.models.BpWorkflowScheme scheme) {
		this.scheme = scheme;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getConditionsCheckClass() {
		return conditionsCheckClass;
	}

	public void setConditionsCheckClass(String conditionsCheckClass) {
		this.conditionsCheckClass = conditionsCheckClass;
	}

	public String getOnChangeClass() {
		return onChangeClass;
	}

	public void setOnChangeClass(String onChangeClass) {
		this.onChangeClass = onChangeClass;
	}


	/**
	 * Both instances of WorkflowNode must be initialized and must contain id.
	 */
	@Override
	public boolean equals(Object obj) {
		if (null==obj) {
			return false;
		}
		if (!obj.getClass().isInstance(BpWorkflowNode.class)) {
			return false;
		}
		BpWorkflowNode wn = (BpWorkflowNode) obj;
		if (null==wn.getId()||null==wn.getStatusName()||null==this.id||null==this.statusName) {
			return false;
		}
		if (this.id.equals(wn.getId())&&this.statusName.equals(wn.getStatusName())) {
			return true;
		} else {
			return false;
		}
	}
}
