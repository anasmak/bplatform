package com.bplatform.server.DAO.models;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.envers.AuditOverride;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

/**
 * Mock to test workflow.
 * <p>
 * Good practice is to get rid of MtM in final buisness entities and map all properties OtM as standalone prop.entity.
 * This approach will allow to use auditing for this entities with Envers in proper and costless way.  
 * <p>
 * Audit taken from:
 * http://docs.jboss.org/envers/docs/
 * http://www.warski.org/blog/category/envers/ (developer blog)
 * http://vuknikolic.me/2011/01/09/hibernate-audit-logging-the-easy-way/ (easy sample)   
 * http://habrahabr.ru/post/120631/
 * http://docs.jboss.org/hibernate/orm/4.2/devguide/en-US/html_single/#d5e3941 (now it's a part of hiber., actual doc!)
 * <p>
 * @author Minolan
 */

@Entity
@Table(name = "mock_document")
@AuditOverride(forClass=BpExtendedEntity.class) //overrides absent audited state "not audited" of extended ancestor entity, somehow mapped "id" is audited automatically
@Audited(targetAuditMode=RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag=true) //"targetAuditMode"- MUST BE IF joined tables (BpUser mapped in superclass) not audited
public class MockDocument extends BpStatefulEntity {
	
	private String someTestField;

	public String getSomeTestField() {
		return someTestField;
	}

	public void setSomeTestField(String someTestField) {
		this.someTestField = someTestField;
	}
	
}