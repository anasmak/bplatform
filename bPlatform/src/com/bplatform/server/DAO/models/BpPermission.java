package com.bplatform.server.DAO.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.springframework.security.core.GrantedAuthority;

/**
 * 
 * Class for storing some security permissions.
 * 
 * @author Anastasia Smakovska
 * 
 * 
 */
@Entity
@Table(name = "BP_PERMISSION", uniqueConstraints = { @UniqueConstraint(columnNames = { "name" }) })
public class BpPermission extends BpTree implements GrantedAuthority {

	private static final long serialVersionUID = -4400060927093228145L;

	@Override
	public String getAuthority() {
		StringBuilder builder = new StringBuilder(permCode);
		builder.append(' ');
		builder.append(name);
		return builder.toString();
	}

	/**
	 * If someone creates new instance without specifying needed fields - it must be his/her own error, we must not allow to save such a retarded entity to DB!  
	 */
	public BpPermission() {}

	public BpPermission(String name) {
		super(name);
	}

	public BpPermission(String ID, String name, String parentID, String tCode,
			String connectedTo, String permCode) {
		super(ID, name, parentID, tCode, connectedTo);
		this.permCode = permCode;
	}

	public BpPermission(String ID, String name, String parentID, String tCode,
			String connectedTo) {
		super(ID, name, parentID, tCode, connectedTo);
	}

	@Column(length = 32)
	private String permCode;

	// Note: you should use full path of targetEntity class, because otherwise
	// Hibernate 4.2 can't determine class path and doesn't throw an exception
	// in this case. It looks ugly, but don't remove class path!
	// Remember: mappedBy and targetEntity is used for avoiding creation excess
	// tables by Hibernate. Instead of this only special columns for id are
	// added to tables.

	// Annotation for generating new table for storing BpPermissionGroup to
	// BpPermission relation in the database
	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "permissions", targetEntity = com.bplatform.server.DAO.models.BpPermissionGroup.class)
	private Set<com.bplatform.server.DAO.models.BpPermissionGroup> bpPermissionsGroups = new HashSet<com.bplatform.server.DAO.models.BpPermissionGroup>();

	// Annotation for storing relation between BpPermission and
	// JoinedBpUserToBpPermission (service table, which stores two extra
	// attributes: isPermitted and isInherited). So, many-to-many relation
	// between BpUser and BpPermission is organized using
	// JoinedBpUserToBpPermission entity.
	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "bpPermission", targetEntity = com.bplatform.server.DAO.models.JoinedBpUserToBpPermission.class, fetch=FetchType.LAZY)
	private Set<com.bplatform.server.DAO.models.JoinedBpUserToBpPermission> joinedUsers = new HashSet<com.bplatform.server.DAO.models.JoinedBpUserToBpPermission>();

	// not interesting: getters and setters
	public String getPermCode() {
		return permCode;
	}

	public void setPermCode(String permCode) {
		this.permCode = permCode;
	}

	public void setPermissionsGroups(
			Set<com.bplatform.server.DAO.models.BpPermissionGroup> permissionsGroups) {
		if (permissionsGroups == null) {
			this.bpPermissionsGroups = new HashSet<com.bplatform.server.DAO.models.BpPermissionGroup>();
		} else {
			this.bpPermissionsGroups = permissionsGroups;
		}
	}

	public Set<com.bplatform.server.DAO.models.BpPermissionGroup> getBpPermissionsGroups() {
		return bpPermissionsGroups;
	}

	public void setBpPermissionsGroups(
			Set<com.bplatform.server.DAO.models.BpPermissionGroup> bpPermissionsGroups) {
		if (bpPermissionsGroups == null) {
			this.bpPermissionsGroups = new HashSet<com.bplatform.server.DAO.models.BpPermissionGroup>();
		} else {
			this.bpPermissionsGroups = bpPermissionsGroups;
		}
	}

	public boolean addJoinedUser(JoinedBpUserToBpPermission joinedUser) {
		return joinedUsers.add(joinedUser);
	}

	public boolean addBpPermissionGroup(BpPermissionGroup permissionGroup) {
		return bpPermissionsGroups.add(permissionGroup);
	}

	public Set<com.bplatform.server.DAO.models.JoinedBpUserToBpPermission> getJoinedUsers() {
		return joinedUsers;
	}

	public void setJoinedUsers(
			Set<com.bplatform.server.DAO.models.JoinedBpUserToBpPermission> joinedUsers) {
		this.joinedUsers = joinedUsers;
	}

}
