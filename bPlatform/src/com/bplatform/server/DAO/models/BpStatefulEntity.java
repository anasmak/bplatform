package com.bplatform.server.DAO.models;

import javax.annotation.Nonnull;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

/**
 * Class to extend when making a stateful entity used by workflow
 * @author Minolan
 */
@MappedSuperclass
@Audited(targetAuditMode=RelationTargetAuditMode.NOT_AUDITED, withModifiedFlag=true)
public class BpStatefulEntity extends BpExtendedEntity implements Cloneable {
	
	@Column(unique = true)
	private String name;
	
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, optional=false)
	@JoinColumn(name = "state_id")
	private BpWorkflowNode state;
	
	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE}, fetch=FetchType.LAZY )
	@JoinColumn(name = "blocked_by")
	private com.bplatform.server.DAO.models.BpUser blockedBy = new com.bplatform.server.DAO.models.BpUser();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BpWorkflowNode getState() {
		return state;
	}

	public void setState(BpWorkflowNode state) {
		this.state = state;
	}

	public com.bplatform.server.DAO.models.BpUser getBlockedBy() {
		return blockedBy;
	}

	public void setBlockedBy(com.bplatform.server.DAO.models.BpUser blockedBy) {
		this.blockedBy = blockedBy;
	}
		
}
