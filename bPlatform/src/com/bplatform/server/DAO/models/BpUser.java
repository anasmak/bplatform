package com.bplatform.server.DAO.models;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.bplatform.server.features.PropertiesFacade;

@NamedQueries({ 
	@NamedQuery(name = "getBpUserByLogin", query = "FROM BpUser user WHERE user.login = :login")
// note: in the HQL should be used class name																							
// and property name of the mapped @Entity instead of the actual table name and
// column name
 })
/**
 * 
 * Class for storing users' information.
 * 
 * @author Anastasia Smakovska
 * 
 */
@Entity
@Table(name = "BP_USER",
uniqueConstraints = {@UniqueConstraint(columnNames={"login"})})
public class BpUser extends BpDefaultEntity {

	@Inject 
	private static PropertiesFacade pFacade;
	
	@Column(nullable = false, length = 32)
	private String login;
	@Column(nullable = false, length = 32)
	private String password;
	@Column(nullable = false, length = 64)
	private String fio;
	@Column(nullable = false, length = 32)
	private String email;
	@Column(length = 2)
	private String locale = null;
	

	// Note: you should use full path of targetEntity class, because otherwise
	// Hibernate 4.2 can't determine class path and doesn't throw an exception
	// in this case. It looks ugly, but don't remove class path!
	// Remember: mappedBy and targetEntity is used for avoiding creation excess
	// tables by Hibernate. Instead of this only special columns for id are
	// added to tables.

	// Annotation for generating new table for storing BpUser to
	// BpUserGroups relation in the database
//	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER, mappedBy = "users", targetEntity = com.bplatform.server.DAO.models.BpUserGroup.class)
//	private Set<com.bplatform.server.DAO.models.BpUserGroup> bpUserGroups = new HashSet<com.bplatform.server.DAO.models.BpUserGroup>();
	
//	private Set<com.bplatform.server.DAO.models.BpUserGroup> bpUserGroups = new HashSet<com.bplatform.server.DAO.models.BpUserGroup>();
	
	@ManyToMany(fetch = FetchType.LAZY )
	@JoinTable(name = "BP_USERS_TO_BP_USER_GROUPS", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "group_id", referencedColumnName = "id"))
	private Set<com.bplatform.server.DAO.models.BpUserGroup> bpUserGroups = new HashSet<com.bplatform.server.DAO.models.BpUserGroup>();
	
	public Set<com.bplatform.server.DAO.models.BpUserGroup> getBpUserGroups() {
		return bpUserGroups;
	}

	public void setBpUserGroups(
			Set<com.bplatform.server.DAO.models.BpUserGroup> bpUserGroups) {
		if(bpUserGroups == null){
			this.bpUserGroups = new HashSet<com.bplatform.server.DAO.models.BpUserGroup>();
		}else{
			this.bpUserGroups = bpUserGroups;
		}
		
	}
	
	// Annotation for storing relation between BpPermission and
	// JoinedBpUserToBpPermission (service table, which stores two extra
	// attributes: isPermitted and isInherited). So, many-to-many relation
	// between BpUser and BpPermission is organized using
	// JoinedBpUserToBpPermission entity.
	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "bpUser", targetEntity = com.bplatform.server.DAO.models.JoinedBpUserToBpPermission.class, fetch = FetchType.LAZY)
	private Set<com.bplatform.server.DAO.models.JoinedBpUserToBpPermission> joinedPermissions = new HashSet<com.bplatform.server.DAO.models.JoinedBpUserToBpPermission>(); 

	public Set<com.bplatform.server.DAO.models.JoinedBpUserToBpPermission> getJoinedPermissions() {
		return joinedPermissions;
	}

	public void setJoinedPermissions(
			Set<com.bplatform.server.DAO.models.JoinedBpUserToBpPermission> permissions) {
		if(permissions == null){
			this.joinedPermissions = new HashSet<com.bplatform.server.DAO.models.JoinedBpUserToBpPermission>();
		}else{
			this.joinedPermissions = permissions;
		}
	}

	BpUser() {
	}

	public BpUser(String login, String password, String email, String fio) {
		super();
		this.login = login;
		this.password = password;
		this.fio = fio;
		this.email = email;
	}

	public BpUser(String login, String password, String fio, String email,
			String locale) {
		super();
		this.login = login;
		this.password = password;
		this.fio = fio;
		this.email = email;
		this.locale = locale;
	}


	//not interesting
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public String getFIO() {
		return fio;
	}

	public void setFIO(String fio) {
		this.fio = fio;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean addJoinedPermission(
			JoinedBpUserToBpPermission joinedPermission) {
		return joinedPermissions.add(joinedPermission);
	}

	public String getFio() {
		return fio;
	}

	public void setFio(String fio) {
		this.fio = fio;
	}

	public boolean addBpUserGroup(BpUserGroup userGroup) {
		return bpUserGroups.add(userGroup);
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

}