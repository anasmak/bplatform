package com.bplatform.server.DAO.models;

import java.util.Date;
import java.util.Random;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;

@NamedQueries({
	@NamedQuery(name = "getTokenByUsername", query = "FROM BpToken token WHERE token.username = :authUsername"),
	})


/**
 * Class for implementing persistent based token approach
 *
 * @see <a href="http://jaspan.com/improved_persistent_login_cookie_best_practice"> Barry Jaspan. Improved Persistent Login Cookie Best Practice </a>
 * @author Anastasia Smakovska
 *
 */
@Entity
@Table(name = "BP_TOKEN")
public class BpToken extends BpDefaultEntity {

	public BpToken(BpUser user) {
		username = user.getLogin();
		Random random = new Random();
		series = new Long(random.nextLong()).toString();
		tokenValue = series;
		date = new Date();
	}

	public BpToken(PersistentRememberMeToken token) {
		this.username = token.getUsername();
		this.series = token.getSeries();
		this.tokenValue = token.getTokenValue();
		this.date = token.getDate();
	}

	@Column(nullable = false, length = 32)
	private String username;

	@Column(nullable = false, length = 64)
	protected String series;

	@Column(nullable = false, length = 64)
	protected String tokenValue;

	// When login was set
	@Column(nullable = false)
	private Date date;

	public void setSeries(String series) {
		this.series = series;
	}

	public void setTokenValue(String tokenValue) {
		this.tokenValue = tokenValue;
	}

	public String getSeries() {
		return series;
	}

	public String getTokenValue() {
		return tokenValue;
	}

	public Date getDate() {
		return date;
	}

	public String getUsername() {
		return username;
	}

	@Override
	public String toString() {
		return "BpToken [username=" + username + ", series=" + series
				+ ", tokenValue=" + tokenValue + ", date=" + date + ", id="
				+ id + "]";
	}

	
}
