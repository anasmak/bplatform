package com.bplatform.server.DAO.models;

import javax.annotation.Nonnull;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries({ 
	@NamedQuery(
			name = "getJoinedByPermission", 
			query = "FROM JoinedBpUserToBpPermission joined WHERE joined.bpPermission = :bpPermission"),
	 @NamedQuery(
			 name="getJoinedByUser", 
			 query = "FROM JoinedBpUserToBpPermission joined WHERE joined.bpUser = :bpUser")
//note: in the HQL should be used class name																							
//and property name of the mapped @Entity instead of the actual table name and
//column name
})
/**
 * 
 * Class for storing many-to-many relation between BpUser and
 * BpPermission with two extra fields: isInherited and isPermitted.
 * 
 * @author Anastasia Smakovska
 * 
 */

@Entity
@Table(name = "BP_PERMISSIONS_TO_BP_USERS")
public class JoinedBpUserToBpPermission extends BpDefaultEntity {

	@Column(name = "is_permitted")
	private boolean isPermitted;

	@Column(name = "is_inherited")
	private boolean isInherited;

	public JoinedBpUserToBpPermission(BpUser bpUser, BpPermission bpPermission,
			boolean isPermitted, boolean isInherited) {
		super();
		this.bpUser = bpUser;
		this.bpPermission = bpPermission;
		this.isPermitted = isPermitted;
		this.isInherited = isInherited;
	}

	// Standard constructor for Hibernate, it's odd according logic of
	// many-to-many relation (each object of this class should have aggregated
	// BpUser and BpPermission instance), but should be for framework needs.
	@Deprecated
	public JoinedBpUserToBpPermission() {
		super();
	}

	// Create relation using Hibernate.
	@Nonnull
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "user_id")
	private BpUser bpUser;

	@Nonnull
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "permission_id")
	private BpPermission bpPermission;

	// not interesting
	public boolean isInherited() {
		return isInherited;
	}

	public BpUser getBpUser() {
		return bpUser;
	}

	public void setInherited(boolean isInherited) {
		this.isInherited = isInherited;
	}

	public void setBpUser(BpUser bpUser) {
		this.bpUser = bpUser;
	}

	public boolean isPermitted() {
		return isPermitted;
	}

	public void setPermitted(boolean isPermitted) {
		this.isPermitted = isPermitted;
	}

	public BpPermission getBpPermission() {
		return bpPermission;
	}

	public void setBpPermission(BpPermission bpPermission) {
		this.bpPermission = bpPermission;
	}
}
