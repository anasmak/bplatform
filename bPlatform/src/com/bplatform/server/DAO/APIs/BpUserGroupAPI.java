package com.bplatform.server.DAO.APIs;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.annotations.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;

import com.bplatform.server.DAO.HibernateFacade;
import com.bplatform.server.DAO.models.BpPermissionGroup;
import com.bplatform.server.DAO.models.BpUser;
import com.bplatform.server.DAO.models.BpUserGroup;
import com.bplatform.shared.exceptions.DataBaseException;

/**
 * 
 * @author Anastasia Smakovska
 *
 * 4 ����. 2013
 *
 */
@ApplicationScoped
public class BpUserGroupAPI {


	@Inject
	private HibernateFacade hibFacade;
	
	public BpUserGroupAPI(){}

	public void delete(BpUserGroup userGroup) throws DataBaseException{
		Session hbSession = hibFacade.getHibernateSession();
		Transaction transaction = null;
		try {
			transaction = hbSession.getTransaction();
			transaction.begin();
			hbSession.delete(userGroup);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DataBaseException("user group cannot be deleted from the database by unresolved issue");
		} finally {
			hbSession.close();
		}
	}

	public void save(BpUserGroup userGroup) throws ConstraintViolationException, DataBaseException {
		Session hbSession = hibFacade.getHibernateSession();
		Transaction transaction = null;
		try {
			transaction = hbSession.getTransaction();
			transaction.begin();
			hbSession.save(userGroup);
			transaction.commit();
		} catch(ConstraintViolationException e){
			transaction.rollback();
			throw e; // trying to save user group with null value or some field is too long string
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DataBaseException("user group cannot be saved to the database by unresolved issue");
		} finally {
			hbSession.close();
		}
	}
	
	public void saveOrUpdate(BpUserGroup userGroup) throws ConstraintViolationException, DataBaseException {
		Session hbSession = hibFacade.getHibernateSession();
		Transaction transaction = null;
		try {
			transaction = hbSession.getTransaction();
			transaction.begin();
			hbSession.saveOrUpdate(userGroup);
			transaction.commit();
		} catch(ConstraintViolationException e){
			transaction.rollback();
			throw e; // trying to save user group with null value or some field is too long string
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DataBaseException("user group cannot be saved to the database by unresolved issue");
		} finally {
			hbSession.close();
		}
	}
	
	public void update(BpUserGroup userGroup) throws ConstraintViolationException, DataBaseException {
		Session hbSession = hibFacade.getHibernateSession();
		Transaction transaction = null;
		try {
			transaction = hbSession.getTransaction();
			transaction.begin();
			//This operation cascades to associated instances if the association is mapped with cascade="save-update".
			hbSession.update(userGroup);
			transaction.commit();
		} catch(ConstraintViolationException e){
			transaction.rollback();
			throw e; // trying to update user group with null value or some field is too long string
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DataBaseException("user group cannot be updated to the database by unresolved issue");
		} finally {
			hbSession.close();
		}
	}
	
	public BpUserGroup getById(String id) throws DataBaseException {
		BpUserGroup userGroup = null;
		Session hbSession = hibFacade.getHibernateSession();
		try {
			userGroup = (BpUserGroup)hbSession.get(BpUserGroup.class, id);
			userGroup.getUsers();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException("user group cannot be recieved from the database by unresolved issue");
		} finally {
			hbSession.close();
		}
		return userGroup;
	}
	
	public Set<String> getByIdUserList(String id) throws DataBaseException{
		BpUserGroup userGroup = null;
//		List<String> users = null;
		Session hbSession = hibFacade.getHibernateSession();
		Set<String> bpUserSet = new HashSet<String>();
		try {
			userGroup = (BpUserGroup)hbSession.get(BpUserGroup.class, id);
			userGroup.getUsers();
			
			for (BpUser i : userGroup.getUsers()) {
				bpUserSet.add(i.getLogin());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException("user group cannot be recieved from the database by unresolved issue");
		} finally {
			hbSession.close();
		}
		return bpUserSet;
	}
	
	@SuppressWarnings("unchecked")
	public List<BpUserGroup> getAllGroups() throws DataBaseException{
		List<BpUserGroup> allGroups = null;
		Session hbSession = hibFacade.getHibernateSession();
		try {
			allGroups = hbSession.createCriteria(BpUserGroup.class).list();
			
//			SQLQuery query = (SQLQuery) hbSession
//					.createSQLQuery("select group_id from BP_USERS_TO_BP_USER_GROUPS where user_id like :name");
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException("user group cannot be recieved from the database by unresolved issue");
		} finally {
			hbSession.close();
		}
		return allGroups;
	}
	
	/**
	 * Select children Groups of Group
	 * @param id of Group
	 * @return Children Groups
	 * @throws DataBaseException
	 */
	@SuppressWarnings("unchecked")
	public List<BpUserGroup> getChildrenGroups(String id) throws DataBaseException{
		List<BpUserGroup> groupChildList = null;
		Session hbSession = hibFacade.getHibernateSession();
		try {
			if (id == null) {
				groupChildList = (List<BpUserGroup>) hbSession.createCriteria(BpUserGroup.class).add(Restrictions.isNull("parentId")).list();
			} else {
				groupChildList = hbSession.createCriteria(BpUserGroup.class).add(Restrictions.eq("parentId", id)).list();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException("Child group cannot be recieved from the database by unresolved issue");
		} finally {
			hbSession.close();
		}
		return groupChildList;
	}
	
	public BpUserGroup getGroupByName (String groupName) throws DataBaseException{
		BpUserGroup bpUserGroup = null;
		Session hbSession = hibFacade.getHibernateSession();
		try {
			bpUserGroup = (BpUserGroup) hbSession
					.createCriteria(BpUserGroup.class)
					.add(Restrictions.eq("name", groupName)).uniqueResult();		
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException("Child group cannot be recieved from the database by unresolved issue");
		} finally {
			hbSession.close();
		}
		return bpUserGroup;
	}
	
	public List<BpUser> initializeBpUsers(BpUserGroup userGroup){
		List<BpUser> users;
		Session hbSession = hibFacade.getHibernateSession();
		try{
			users = (List<BpUser>)hbSession.createCriteria(BpUser.class, "user").createAlias("user.bpUserGroups", "userGroup").add(Restrictions.eq("userGroup.id", userGroup.getId())).list();
			userGroup.setUsers(new HashSet<BpUser>(users));
		}catch(Exception e){
			e.printStackTrace();
			throw new DataBaseException("BpUserGroups cannot be recieved from the database by unresolved issue");
		} finally{
			hbSession.close();
		}
		return users;
	}

	public List<BpPermissionGroup> initializeBpPermissionGroups(BpUserGroup userGroup){
		List<BpPermissionGroup> permissionGroups;
		Session hbSession = hibFacade.getHibernateSession();
		try{
			permissionGroups = (List<BpPermissionGroup>)hbSession.createCriteria(BpPermissionGroup.class, "permissionGroup").createAlias("permissionGroup.bpUserGroups", "userGroup").add(Restrictions.eq("userGroup.id", userGroup.getId())).list();
			userGroup.setPermissionGroups(new HashSet<BpPermissionGroup>(permissionGroups));
		}catch(Exception e){
			e.printStackTrace();
			throw new DataBaseException("BpUserGroups cannot be recieved from the database by unresolved issue");
		} finally{
			hbSession.close();
		}
		return permissionGroups;
	}
	
}
