package com.bplatform.client.authModule;


import com.bplatform.client.authModule.ui.LoginGreeting;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class AuthModule implements EntryPoint {

	private LoginGreeting logIn = new LoginGreeting();
//	private FileUploadForm fp = new FileUploadForm();
//	private SystemProperties acc = new SystemProperties();
//	private UserAdminForm logIn = new UserAdminForm();
	
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {

		RootPanel.get().add(logIn.asWidget());
//		RootPanel.get().add(fp.asWidget());
//		RootPanel.get().add(acc.accButton());
//		RootPanel.get().add(logIn.asWidget());
		
		//fp.showWindow();
	}
}
