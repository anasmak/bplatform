package com.bplatform.client.authModule.services;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface AuthServiceAsync {
	
	void userAuth(String login, String password, AsyncCallback<String> asyncCallback);

}
