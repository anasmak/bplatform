package com.bplatform.client.authModule.services;

import com.bplatform.shared.exceptions.BpAuthenticationException;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
//import com.google.gwt.user.client.rpc.ServiceDefTarget;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("../auth")
public interface AuthService extends RemoteService {
	
	public static class Util {
		private static AuthServiceAsync instance;

		public static AuthServiceAsync getInstance() {
			if (instance == null) {
				instance = GWT.create(AuthService.class);
				GWT.log("loading rpc", null);
			}
			return instance;
		}
	}

	String userAuth(String login, String password) throws BpAuthenticationException;
	
}
