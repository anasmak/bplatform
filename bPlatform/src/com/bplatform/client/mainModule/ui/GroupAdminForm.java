/**
 * Create user profile Interface  
 * @author RomanL
 */
package com.bplatform.client.mainModule.ui;


import java.util.List;

import com.bplatform.client.bpShared.DTO.BpGroupDTO;
import com.bplatform.client.bpShared.platform.RpcCallback;
import com.bplatform.client.mainModule.services.GroupAdminService;
import com.bplatform.client.mainModule.services.GroupAdminServiceAsync;
import com.bplatform.client.mainModule.ui.widgets.CheckBoxTree;
import com.bplatform.client.mainModule.ui.widgets.RpcWaitingWindow;
import com.bplatform.client.mainModule.ui.widgets.UserList;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.TabPanel;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.container.MarginData;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.form.validator.MaxLengthValidator;
import com.sencha.gxt.widget.core.client.form.validator.MinLengthValidator;
import com.sencha.gxt.widget.core.client.form.validator.RegExValidator;

/**
 * @author IhorG
 * 
 */
public class GroupAdminForm implements IsWidget {

	private Window window;
	private ContentPanel userSettingsOwn;
	private TextField name;
	private CheckBoxTree checkBoxTree = new CheckBoxTree();
	private GroupAdminServiceAsync groupAdminSvc = GWT
			.create(GroupAdminService.class);
	private UserList userlist = new UserList();
	
	private String userGroupId = null;
	
	public void setUserGroupId(String userGroupId) {
		this.userGroupId = userGroupId;
	}
	
	public String getUserGroupId() {
		return this.userGroupId;
	}

	// Show own user Properties
	@Override
	public Widget asWidget() {
		
		createTabPanel(null);
		return userSettingsOwn;
		
	}

	// Show user Properties
	public Widget asWidget(String groupId) {
		
//		createTabPanel(groupId);
//		window = new Window();
//		window.setPixelSize(700, 500);
//		window.setModal(true);
//		window.setBlinkModal(true);
//		window.setHeadingText("User Info");
//		window.setHeaderVisible(true);
//		window.add(userSettingsOwn);
//
//		return window;
		
		createTabPanel(groupId);
		return userSettingsOwn;

	}

	private void createTabPanel(String groupId) {

		userSettingsOwn = new ContentPanel();
		userSettingsOwn.setHeaderVisible(false);

		TabPanel tabs = new TabPanel();
		VerticalLayoutContainer p = new VerticalLayoutContainer();
		p.setLayoutData(new MarginData(8));
		tabs.add(p, "Group Settings");
		tabs.addStyleName("listStyle");

		// Name field
//		groupIdField = new TextField();
//		groupIdField.hide();
//		p.add(new FieldLabel(groupIdField, "groupId"), new VerticalLayoutData(1, -1));
				
		// Name field
		name = new TextField();
		name.setAutoValidate(true);
		name.addValidator(new MinLengthValidator(3));
		name.addValidator(new MaxLengthValidator(30));
		name.addValidator(new RegExValidator("^[^!#$%&'*+/=?^`{|}~]+$", "Field match illegal symbol"));
		p.add(new FieldLabel(name, "Name"), new VerticalLayoutData(1, -1));

		p = new VerticalLayoutContainer();
		p.setLayoutData(new MarginData(8));
		tabs.add(p, "Member of Groups");
		p.add(checkBoxTree.asWidget(groupId, false, true));
		
		if (groupId != null) {
			p = new VerticalLayoutContainer();
			p.setLayoutData(new MarginData(8));
			tabs.add(p, "Users from Group");
			p.add(userlist.usersListWidget(groupId));
			getGroupProperties(groupId);
			//p.add(checkBoxTree.usersList(groupId));
		}
		
		// Get Group Properties

		Button ok = new Button("OK");

		ok.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if ((name.getText() == null) || (name.getText().isEmpty()) ||  !name.isValid()) {
					name.isValid(false);
					com.google.gwt.user.client.Window.alert("Please type Group Name!");
					name.setAllowBlank(false);
				} else {
					saveGroupProperties(name.getText());
				}
			}
		});
		userSettingsOwn.addButton(ok);

		userSettingsOwn.add(tabs);
	}

	// Get Group properties
	public void getGroupProperties(String groupId) {

		final RpcWaitingWindow rpcWait = new RpcWaitingWindow();
		rpcWait.showWindow();
		
		if (groupAdminSvc == null) {
			groupAdminSvc = GWT.create(GroupAdminService.class);
		}

		groupAdminSvc.getGroupProperties(groupId, new RpcCallback<BpGroupDTO>(rpcWait.getBox()) {

					@Override
					public void onSuccess(BpGroupDTO result) {
						
						name.setText(result.getName());
						setUserGroupId(result.getGroupId());
						
						rpcWait.deleteWindow();
						
					}
				});

	}

	/**
	 * Save Group properties
	 * @param name
	 */
	public void saveGroupProperties(String name) {
		
		if (groupAdminSvc == null) {
			groupAdminSvc = GWT.create(GroupAdminService.class);
		}
		
		try {
			BpGroupDTO bpGroupProp = new BpGroupDTO();
			List<BpGroupDTO> checkedGroup = checkBoxTree.getChekedItems();
			
			bpGroupProp.setName(name);
			
			if ((checkedGroup == null) || checkedGroup.get(0).getParentId() == null || checkedGroup.isEmpty()) {
				com.google.gwt.user.client.Window.alert("Please select parent Group!");
			} else if (checkedGroup.size() > 1) {
				com.google.gwt.user.client.Window.alert("Group can be member only one parent!");
			} else {
				
					bpGroupProp.setParentId(checkedGroup.get(0).getParentId());
					
					if (getUserGroupId() != null) {
						bpGroupProp.setGroupId(getUserGroupId());
					}
					
					final RpcWaitingWindow rpcWait = new RpcWaitingWindow();
					rpcWait.showWindow();

					groupAdminSvc.addOrUpdateGroup(bpGroupProp,
							new RpcCallback<String>(rpcWait.getBox()) {

								@Override
								public void onSuccess(String result) {
									rpcWait.deleteWindow();
									com.google.gwt.user.client.Window.alert(result);
								}
							});
			}
			
		} catch (Exception e) {
			com.google.gwt.user.client.Window.alert("Please select parent Group!");
		} 
		
	
	}

	public void show() {
		window.show();
	}

}
