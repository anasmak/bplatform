package com.bplatform.client.mainModule.ui.widgets;

import com.google.gwt.user.client.Timer;
import com.sencha.gxt.widget.core.client.box.AutoProgressMessageBox;

/**
 * @author IhorG
 *
 */
public class RpcWaitingWindow {
	private final AutoProgressMessageBox box = new AutoProgressMessageBox ("Progress", "Loading data, please wait...");
	
	/**
	 * Insert progress bar before RPC OnSuccess method
	 */
	public void showWindow() {
		box.setProgressText("Progress...");
        box.auto();
        box.show();
	}

	/**
	 * Insert progress bar inside RPC OnSuccess method
	 */
	public void deleteWindow() {
		Timer t = new Timer() {
            @Override
            public void run() {
              if (box != null) {
            	box.hide();}
            }
          };
          t.schedule(1000);
	}
	
	/**
	 * Insert progress bar after RPC OnSuccess method
	 */
	public void verifyWindow() {
		Timer t = new Timer() {
            @Override
            public void run() {
            	if (box.isVisible())
            	{
            	box.hide();
//             	Info.display("Sorry...", "Data can't be loaded. Please try little bit later!");
            	com.google.gwt.user.client.Window.alert("Sorry... Data can't be loaded. Please try little bit later!");
            	}
            }
          };
          t.schedule(10000);
	}
	
	public AutoProgressMessageBox getBox() {
		return box;	
	}
}
