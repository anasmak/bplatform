package com.bplatform.client.mainModule.ui.widgets;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.container.FlowLayoutContainer;

import java.util.List;

import com.bplatform.client.bpShared.DTO.BpGroupDTO;
import com.bplatform.client.bpShared.DTO.BpUserDTO;
import com.bplatform.client.bpShared.DTO.BpUserGroupFoldersDTO;
import com.bplatform.client.bpShared.platform.RpcCallback;
import com.bplatform.client.mainModule.services.GroupAdminService;
import com.bplatform.client.mainModule.services.GroupAdminServiceAsync;
import com.bplatform.client.mainModule.services.UserAdminService;
import com.bplatform.client.mainModule.services.UserAdminServiceAsync;
import com.bplatform.client.mainModule.ui.GroupAdminForm;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.core.client.util.DelayedTask;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.TreeStore;
import com.sencha.gxt.widget.core.client.container.MarginData;
import com.sencha.gxt.widget.core.client.event.CheckChangedEvent;
import com.sencha.gxt.widget.core.client.event.CheckChangedEvent.CheckChangedHandler;
import com.sencha.gxt.widget.core.client.tree.Tree;
import com.sencha.gxt.widget.core.client.tree.Tree.CheckCascade;
import com.sencha.gxt.widget.core.client.tree.Tree.CheckState;

/**
 * Generate Check Box Groups Tree
 * 
 * @author IhorG
 */
public class CheckBoxTree implements IsWidget {

	private UserAdminServiceAsync userPropertiesSvc = GWT
			.create(UserAdminService.class);
	private GroupAdminServiceAsync groupPropertiesSvc = GWT
			.create(GroupAdminService.class);
	private TreeStore<BpGroupDTO> store = new TreeStore<BpGroupDTO>(new KeyProvider());
	private FlowLayoutContainer con = new FlowLayoutContainer();
	private ContentPanel viewPropPanel = new ContentPanel();
	private ContentPanel viewPropPanel2 = new ContentPanel();
	
	public ContentPanel getViewPropPanel() {
		return viewPropPanel;
	}
	
	public ContentPanel getViewPropPanel2() {
		return viewPropPanel2;
	}
	
	/**
	 * Get own tree of groups
	 */
	@Override
	public Widget asWidget() {
		groupsWidget(null, false, false);
		return con;
	}

	/**
	 * Get tree of groups by login
	 * @param userLogin
	 * @return FlowLayoutContainer
	 */
	public Widget asWidget(String itemIdentifier, boolean Selectable) {
		groupsWidget(itemIdentifier, Selectable, false);
		return con;
	}

	public Widget asWidget(String itemIdentifier, boolean Selectable, boolean groupTreeCheck) {
		groupsWidget(itemIdentifier, Selectable, groupTreeCheck);
		return con;
	}

	
	public Widget groupsWidget(String itemIdentifier, final boolean selectable, final boolean groupTreeCheck) {
		
		if (selectable == true) {
			getTree().setCheckable(false);
		} else {
			getTree().setCheckable(true);
		}

		getTree().setCheckStyle(CheckCascade.NONE);
		getTree().setAutoLoad(true);

		// Get all groups
		getAllGroups(itemIdentifier, selectable, groupTreeCheck);
		
		final DelayedTask task = new DelayedTask() {

			@Override
			public void onExecute() {
//				Info.display("Action", "Check changed: "
//						+ getTree().getCheckedSelection().size() + "\n");

				// Show user's list
				if (selectable == true && groupTreeCheck == false)
//					getViewPropPanel().add(new GroupAdminForm());
					getViewPropPanel().add(new GroupAdminForm().asWidget(getSelectedItem().getGroupId()));
//					usersList(getSelectedItem().getGroupId());
			}

		};

		if (selectable == false) {
			// ======================Check Event
			getTree().addCheckChangedHandler(new CheckChangedHandler<BpGroupDTO>() {
				@Override
				public void onCheckChanged(CheckChangedEvent<BpGroupDTO> event) {
					task.delay(100);
				}
			});
			// ======================Check Event
		} else {
		// ====================== Start Handler Select Event
		getTree().getSelectionModel().addSelectionHandler(
				new SelectionHandler<BpGroupDTO>() {
					@Override
					public void onSelection(SelectionEvent<BpGroupDTO> event) {
						task.delay(100);
					}
				});
		// ====================== End Handler Select Event
		}
		con.add(getTree(), new MarginData(10));
		return con;
	}

	public BpGroupDTO getSelectedItem() {
		BpGroupDTO selected = getTree().getSelectionModel().getSelectedItem();
		return selected;
	}

	public List<BpGroupDTO> getChekedItems() {
		List<BpGroupDTO> checked = getTree().getCheckedSelection();
		return checked;
	}

	private void processFolder(TreeStore<BpGroupDTO> store,
			BpUserGroupFoldersDTO folder) {
		for (BpGroupDTO child : folder.getChildren()) {
			store.add(folder, child);
			if (child instanceof BpUserGroupFoldersDTO) {
				processFolder(store, (BpUserGroupFoldersDTO) child);
			}
		}
	}

	class KeyProvider implements ModelKeyProvider<BpGroupDTO> {
		@Override
		public String getKey(BpGroupDTO item) {
			String intToString = Integer.toString(item.getId());
			return (item instanceof BpUserGroupFoldersDTO ? "f-" : "m-")
					+ intToString;
		}
	}

	Tree<BpGroupDTO, String> tree = new Tree<BpGroupDTO, String>(store,
			new ValueProvider<BpGroupDTO, String>() {

				@Override
				public String getValue(BpGroupDTO object) {
					return object.getName();
				}

				@Override
				public void setValue(BpGroupDTO object, String value) {
				}

				@Override
				public String getPath() {
					return "name";
				}
			});
	
	public Tree<BpGroupDTO, String> getTree() {
		this.tree.expandAll();
		return this.tree;
	}

	// GWT user's groups checked box
	public void checkedUserGroups(String userLogin) {
		final RpcWaitingWindow rpcWait = new RpcWaitingWindow();
		rpcWait.showWindow();
		
//		box.setProgressText("Progress...");
//        box.auto();
//        box.show();
        

		// First Method
		// userPropertiesSvc
		// .getUserToGroups(new RpcCallback<List<String>>() {
		//
		// @Override
		// public void onSuccess(List<String> result) {
		// for (String i : result) {
		// for (BpGroupDTO j : store.getAll()) {
		// if (i.equals(j.getGroupId())) {
		// getTree().setChecked(j,
		// CheckState.CHECKED);
		// System.out.println(j);
		// }
		// }
		// System.out.println(i);
		// }
		//
		// }
		//
		// });

		// Second Method
		userPropertiesSvc.getUserProperties(userLogin,
				new RpcCallback<BpUserDTO>(rpcWait.getBox()) {

					@Override
					public void onSuccess(BpUserDTO result) {
						for (BpGroupDTO i : result.getGroups()) {
							for (BpGroupDTO j : store.getAll()) {
								if (i.getGroupId().equals(j.getGroupId())) {
									getTree().setChecked(j, CheckState.CHECKED);
									System.out.println(j);
								}
							}
							System.out.println(i);
						}
						rpcWait.deleteWindow();
					}
				});
	}
	
	// GWT group's groups checked box
	public void checkedGroupGroups(String groupId) {
		final RpcWaitingWindow rpcWait = new RpcWaitingWindow();
		rpcWait.showWindow();
		groupPropertiesSvc.getGroupProperties(groupId,
				new RpcCallback<BpGroupDTO>(rpcWait.getBox()) {

					@Override
					public void onSuccess(BpGroupDTO result) {
							for (BpGroupDTO j : store.getAll()) {
								if (result.getParentId().equals(j.getGroupId())) {
									getTree().setChecked(j, CheckState.CHECKED);
								}
							}
						rpcWait.deleteWindow();
						
					}
				});
	}

	// GWT get tree all user's groups
	public void getAllGroups(final String itemIdentifier, final boolean Selectable, final boolean groupTreeCheck) {
		
		final RpcWaitingWindow rpcWait = new RpcWaitingWindow();
		rpcWait.showWindow();
		
		userPropertiesSvc
				.getGroupsList(new RpcCallback<List<BpUserGroupFoldersDTO>>(rpcWait.getBox()) {

					@Override
					public void onSuccess(List<BpUserGroupFoldersDTO> result) {
						// BPGroupsFolderDTO root = getGroupsRootFolder();

						for (BpUserGroupFoldersDTO root : result) {
							for (BpGroupDTO base : root.getChildren()) {
								store.add(base);
								if (base instanceof BpUserGroupFoldersDTO) {
									processFolder(store,
											(BpUserGroupFoldersDTO) base);
								}

							}
						}
						rpcWait.deleteWindow();
						// Check user's group
						if (Selectable == false && groupTreeCheck == false)
							checkedUserGroups(itemIdentifier);
						else if (Selectable == false && groupTreeCheck == true && itemIdentifier != null)
							checkedGroupGroups(itemIdentifier);
					}
				});
	}
	
}
