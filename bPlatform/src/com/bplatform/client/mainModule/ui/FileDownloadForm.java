package com.bplatform.client.mainModule.ui;

import com.bplatform.client.mainModule.ui.bpmenu.BpElement;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer.BoxLayoutPack;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.FormPanel;
import com.sencha.gxt.widget.core.client.form.FormPanel.Method;
/**
 * �������� ����� � �������
 * @author IhorG
 *
 */
public class FileDownloadForm extends BpElement {
	private Window window;
	public final static String SERVLET_NAME = "services/downloadFileServlet";

	@Override
	public void show() {
		window.show();

	}

	@Override
	public Widget asWidget() {
		if (window == null) {
			createDownPan();
		}
		return window;
	}

	private void createDownPan() {
		window = new Window();
		window.setPixelSize(300, 150);
		window.setHeadingText("File Download");
		window.setButtonAlign(BoxLayoutPack.CENTER);

		final FormPanel form = new FormPanel();
		form.setAction(GWT.getHostPageBaseURL() + SERVLET_NAME);
		form.setMethod(Method.GET);
		// form.setEncoding(Encoding.MULTIPART);

		window.add(form);
		final VerticalPanel p = new VerticalPanel();
		form.add(p);

		TextButton btn = new TextButton("Download");

		btn.addSelectHandler(new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				if (!form.isValid()) {
					return;
				}
				form.submit();

				/*
				 * MessageBox box = new MessageBox("Your file was downloaded.");
				 * box.setIcon(MessageBox.ICONS.info()); box.show();
				 * ��� ������� ����� �������� ����
				 */
				
			}
		});
		window.addButton(btn);

	}

}
