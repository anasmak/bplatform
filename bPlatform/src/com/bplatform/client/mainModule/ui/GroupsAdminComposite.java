package com.bplatform.client.mainModule.ui;

import com.bplatform.client.bpShared.platform.RpcCallback;
import com.bplatform.client.mainModule.services.GroupAdminService;
import com.bplatform.client.mainModule.services.GroupAdminServiceAsync;
import com.bplatform.client.mainModule.ui.widgets.CheckBoxTree;
import com.bplatform.client.mainModule.ui.widgets.RpcWaitingWindow;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.SplitLayoutPanel;

import com.sencha.gxt.widget.core.client.Dialog.PredefinedButton;
import com.sencha.gxt.widget.core.client.box.ConfirmMessageBox;
import com.sencha.gxt.widget.core.client.button.ButtonBar;
import com.sencha.gxt.widget.core.client.container.MarginData;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.HideEvent;
import com.sencha.gxt.widget.core.client.event.HideEvent.HideHandler;

public class GroupsAdminComposite extends ResizeComposite {
	
	private Button addGroupsButton = new Button("Add Group");
	private Button removeGroupButton = new Button("Remove Group");
	private CheckBoxTree checkBoxTree = new CheckBoxTree();
	private SplitLayoutPanel sp = new SplitLayoutPanel();
	private VerticalLayoutContainer vp = new VerticalLayoutContainer(); 
	
	private GroupAdminServiceAsync groupsPropertiesSvc = GWT
			.create(GroupAdminService.class);
	
	public GroupsAdminComposite() {

		checkBoxTree.getViewPropPanel2().setHeadingText("Groups");
//		checkBoxTree.getViewPropPanel2().setSize("100%", "100%");
		checkBoxTree.getViewPropPanel2().add(checkBoxTree.groupsWidget(null, true, false));
		vp.add(checkBoxTree.getViewPropPanel2());
		vp.setWidth("220px");
		vp.setHeight("100%");
		
	    ButtonBar buttonBar = new ButtonBar();
	    buttonBar.setMinButtonWidth(75);
	    buttonBar.getElement().setMargins(0);
	    buttonBar.add(addGroupsButton);
	    buttonBar.add(removeGroupButton);
	    
	    vp.add(buttonBar);
	    
//		vp.add(addGroupsButton);
//		vp.add(removeGroupButton);
		sp.addWest(vp, 220);
		
		if (checkBoxTree.getSelectedItem() == null) {
			checkBoxTree.getViewPropPanel().setHeadingText("Users");
			checkBoxTree.getViewPropPanel().setSize("100%", "100%");
			HTML text = new HTML("Please, select group");
			text.setHeight("200px");
			checkBoxTree.getViewPropPanel().add(text, new MarginData(5));
		}

		sp.add(checkBoxTree.getViewPropPanel());
		sp.setSize("100%", "100%");
		
		addGroupsButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				GroupAdminForm groupAdminForm = new GroupAdminForm();
				groupAdminForm.setUserGroupId(null);
				checkBoxTree.getViewPropPanel().add(groupAdminForm);
			}	
		});
		
		removeGroupButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {

				if (checkBoxTree.getSelectedItem() == null) {
					
					com.google.gwt.user.client.Window.alert("Please select group!");
					
				} else {
					
				      final ConfirmMessageBox box = new ConfirmMessageBox("Confirm", "Are you sure you want to delete item?");
				      
				      box.addHideHandler(new HideHandler() {
				          public void onHide(HideEvent event) {
				            if (box.getHideButton() == box.getButtonById(PredefinedButton.YES.name())) {
				            	deleteGroup(checkBoxTree.getSelectedItem().getGroupId());
				            } else if (box.getHideButton() == box.getButtonById(PredefinedButton.NO.name())){
				              // perform NO action
				            }
				          }
				        });
				      
				      box.show();
				      
				}
			}
		});
		
		initWidget(sp);
				
	}
	
	// GWT delete Group
	public void deleteGroup (String groupId) {
		final RpcWaitingWindow rpcWait = new RpcWaitingWindow();
		rpcWait.showWindow();
			
		groupsPropertiesSvc.deleteGroup(groupId,
				new RpcCallback<String>(rpcWait.getBox()) {

					@Override
					public void onSuccess(String result) {
						rpcWait.deleteWindow();
						com.google.gwt.user.client.Window.alert(result);
					}
				});
	}

}
