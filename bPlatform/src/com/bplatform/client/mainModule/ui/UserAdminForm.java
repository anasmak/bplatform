/**
 * Create user profile Interface  
 * @author RomanL
 */
package com.bplatform.client.mainModule.ui;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.bplatform.client.bpShared.DTO.BpGroupDTO;
import com.bplatform.client.bpShared.DTO.BpUserDTO;
import com.bplatform.client.bpShared.platform.RpcCallback;
import com.bplatform.client.mainModule.services.UserAdminService;
import com.bplatform.client.mainModule.services.UserAdminServiceAsync;
import com.bplatform.client.mainModule.ui.widgets.CheckBoxTree;
import com.bplatform.client.mainModule.ui.widgets.RpcWaitingWindow;
import com.bplatform.shared.validators.PassRetypeValidator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.EditorError;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.TabPanel;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.container.MarginData;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.PasswordField;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.form.validator.MaxLengthValidator;
import com.sencha.gxt.widget.core.client.form.validator.MinLengthValidator;
import com.sencha.gxt.widget.core.client.form.validator.RegExValidator;

/**
 * @author IhorG
 * 
 */
public class UserAdminForm implements IsWidget {

	private Window window;
	private ContentPanel userSettingsOwn;
	private TextField login;
	private PasswordField pass;
	private PasswordField passRetype;
	private TextField name;
	private TextField email;
	private CheckBoxTree checkBoxTree = new CheckBoxTree();
	private UserAdminServiceAsync userOwnSettingsSvc = GWT
			.create(UserAdminService.class);

	// Show own user Properties
	@Override
	public Widget asWidget() {
		// if (userSettingsOwn == null) {
		// createTabPanOwn();
		// }

		createTabPanOwn(null);
		return userSettingsOwn;
	}

	// Show user Properties
	public Window asWidget(String userName) {
		// if (userSettingsOwn == null) {
		// createTabPanOwn();
		// }

		createTabPanOwn(userName);

		window = new Window();
		window.setPixelSize(700, 500);
		window.setModal(true);
		window.setBlinkModal(true);
		window.setHeadingText("User Info");
		window.setHeaderVisible(true);
		window.add(userSettingsOwn);

		return window;

	}

	private void createTabPanOwn(String userLogin) {

		userSettingsOwn = new ContentPanel();
		userSettingsOwn.setHeaderVisible(false);

		TabPanel tabs = new TabPanel();
		VerticalLayoutContainer p = new VerticalLayoutContainer();
		p.setLayoutData(new MarginData(8));
		tabs.add(p, "User Info");
		tabs.addStyleName("listStyle");

		// Login field
		login = new TextField();
		login.setAutoValidate(true);
		login.setEnabled(false);
		login.addValidator(new MinLengthValidator(6));
		login.addValidator(new MaxLengthValidator(15));
		p.add(new FieldLabel(login, "Login"), new VerticalLayoutData(1, -1));

		// Password field
		pass = new PasswordField();
		pass.setAutoValidate(true);
		pass.addValidator(new MinLengthValidator(6));
		pass.addValidator(new MaxLengthValidator(20));
		pass.addValidator(new RegExValidator("^.*(?=.{6,})(?=.*\\d)(?=.*[a-zÐ°-Ñ�Ñ‘Ñ—Ñ–])(?=.*[A-ZÐ�-Ð¯Ð�Ð‡Ð†])(?=.*[â„–;:?/!-_*()|@#$%^&+=]).*$", 
				"Please, enter correct password! Password must numeric symbol, BIG and small letters. Length must be within ranges 6..20"));
		p.add(new FieldLabel(pass, "Password"), new VerticalLayoutData(1, -1));

		// Retype password field
		passRetype = new PasswordField();
		passRetype.setAutoValidate(true);
		passRetype.addValidator(new MinLengthValidator(6));
		passRetype.addValidator(new MaxLengthValidator(15));
		passRetype.addValidator(new PassRetypeValidator() {
			@Override
			public List<EditorError> validate(Editor<String> field, String value) {
				List<EditorError> errors = null;
				if (value != null && (!value.equals(pass.getValue()))) {
					String message = "Passwords not matches!";
					errors = createError(field, message, value);
				}
				return errors;
			}
		});
		passRetype.addValidator(new RegExValidator("^.*(?=.{6,})(?=.*\\d)(?=.*[a-zÐ°-Ñ�Ñ‘Ñ—Ñ–])(?=.*[A-ZÐ�-Ð¯Ð�Ð‡Ð†])(?=.*[â„–;:?/!-_*()|@#$%^&+=]).*$", 
				"Please, enter correct password! Password must numeric symbol, BIG and small letters. Length must be within ranges 6..15"));
		p.add(new FieldLabel(passRetype, "Retype password"), new VerticalLayoutData(1, -1));

		// Name field
		name = new TextField();
		name.setAutoValidate(true);
		name.addValidator(new MinLengthValidator(3));
		name.addValidator(new MaxLengthValidator(30));
		name.addValidator(new RegExValidator("^[^!#$%&'*+/=?^`{|}~]+$", "Field match illegal symbol"));
		p.add(new FieldLabel(name, "Name"), new VerticalLayoutData(1, -1));

		email = new TextField();
		email.setAutoValidate(true);
		email.addValidator(new MinLengthValidator(5));
		email.addValidator(new MaxLengthValidator(50));
		email.addValidator(new RegExValidator("[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+(?:[A-Za-z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)\\b", "Please, enter correct email..."));
		p.add(new FieldLabel(email, "Email"), new VerticalLayoutData(1, -1));

		p = new VerticalLayoutContainer();
		p.setLayoutData(new MarginData(8));
		tabs.add(p, "Member of Groups");

		p.add(checkBoxTree.asWidget(userLogin, false));

		// Get user Properties
		getUserProperties(userLogin);

		Button ok = new Button("OK");

		ok.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				// Save user properties to Data Base
				if (!pass.getText().equals(passRetype.getText())) {
					com.google.gwt.user.client.Window.alert("Passwords don't matches!");
				} else if (name.isValid() && pass.isValid() && email.isValid()) {
					saveUserProperties(login.getText(), pass.getText(), passRetype.getText(), name.getText(), email.getText());
				} else {
					com.google.gwt.user.client.Window.alert("Please, fill correctly all fields!");
				}
				
			}
		});
		userSettingsOwn.addButton(ok);

		userSettingsOwn.add(tabs);
	}

	// Get user properties
	public void getUserProperties(String userLogin) {

		final RpcWaitingWindow rpcWait = new RpcWaitingWindow();
		rpcWait.showWindow();
		
		if (userOwnSettingsSvc == null) {
			userOwnSettingsSvc = GWT.create(UserAdminService.class);
		}

		userOwnSettingsSvc.getUserProperties(userLogin,
				new RpcCallback<BpUserDTO>(rpcWait.getBox()) {

					@Override
					public void onSuccess(BpUserDTO result) {
						login.setText(result.getLogin());
						pass.setText(result.getPassword());
						passRetype.setText(result.getPassword());
						name.setText(result.getFIO());
						email.setText(result.getEmail());
						
						rpcWait.deleteWindow();

					}
				});

	}

	// Save user properties
	public void saveUserProperties(String login, String pass,
			String passRetype, String name, String email) {
		
		final RpcWaitingWindow rpcWait = new RpcWaitingWindow();
		rpcWait.showWindow();
		
		if (userOwnSettingsSvc == null) {
			userOwnSettingsSvc = GWT.create(UserAdminService.class);
		}

		BpUserDTO bpUserProp = new BpUserDTO();

		List<BpGroupDTO> chekedGroups = checkBoxTree.getChekedItems();

		Set<BpGroupDTO> itemGroup = new HashSet<BpGroupDTO>();
		for (int i = 0; i < chekedGroups.size(); i++) {
			itemGroup.add(chekedGroups.get(i));
		}

		bpUserProp.setGroups(itemGroup);
		bpUserProp.setLogin(login);
		bpUserProp.setPassword(pass);
		bpUserProp.setPasswordRetype(passRetype);
		bpUserProp.setFIO(name);
		bpUserProp.setEmail(email);

		userOwnSettingsSvc.setUserProperties(bpUserProp,
				new RpcCallback<String>(rpcWait.getBox()) {

					@Override
					public void onSuccess(String result) {
						rpcWait.deleteWindow();
						com.google.gwt.user.client.Window.alert(result);
					}
				});
	}

	public void show() {
		window.show();
	}

}
