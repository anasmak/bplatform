package com.bplatform.client.mainModule.ui.bpmenu;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.cell.core.client.ButtonCell.IconAlign;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;

public class BpMenuButton implements IsWidget {
	
	final String WIDTH = "150px";
	final String HEIGHT = "150px";
	
	private BpElement elm;
	private TextButton mBtn = new TextButton();

	@Override
	public Widget asWidget() {
		//TODO button optional params from somewhere
		mBtn.setTitle("Some title");
		mBtn.setIconAlign(IconAlign.TOP);
		mBtn.setWidth(WIDTH);
		mBtn.setHeight(HEIGHT);
		mBtn.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				
				elm.show();				
			}
		});
		return mBtn;
	}
	
	public <T extends BpElement> BpMenuButton(BpElement elm) {
		super();
		elm.asWidget();
		this.elm = elm; 
	}
	
	public void onButtonPress() {
		elm.show();
	}
	
	<T extends BpElement> void setBpElement(T elm) {
		this.elm = elm;
	}
	
	public void setBtnImg() {};
}
