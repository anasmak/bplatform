package com.bplatform.client.mainModule.ui.bpmenu;

import com.google.gwt.user.client.ui.Widget;

/**
 * Implement in widgets that are elements of platform and should be accessed through main menu.
 * @author Smak
 *
 */
public abstract class BpElement extends Widget {
	
	public abstract void show();
	
}
