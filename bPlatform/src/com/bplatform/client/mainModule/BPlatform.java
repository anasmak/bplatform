package com.bplatform.client.mainModule;


import com.bplatform.client.mainModule.ui.FileDownloadForm;
import com.bplatform.client.mainModule.ui.FileUploadForm;
import com.bplatform.client.mainModule.ui.SystemProperties;
import com.bplatform.client.mainModule.ui.bpmenu.BpElement;
import com.bplatform.client.mainModule.ui.bpmenu.BpMenuButton;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;


/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class BPlatform implements EntryPoint {
	
	/**
	 * to store elements of BpMenu
	 */
//	List<BpMenuButton> bpMenu = new ArrayList<BpMenuButton>();
	
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		
		//bp menu
		RootPanel menuContainer = RootPanel.get("bm_menu_buttons_layer");
		
		//BPService.Util.getInstance().
		BpElement beSP = GWT.create(SystemProperties.class);
		BpMenuButton bmSP = new BpMenuButton(beSP);
//		beSP.asWidget().setTitle("User Properties");
		menuContainer.add(bmSP.asWidget());
		
		BpElement beFU = GWT.create(FileUploadForm.class);
		BpMenuButton bmFU = new BpMenuButton(beFU);
		bmFU.asWidget().setTitle("File Upload");
		menuContainer.add(bmFU.asWidget());
		
		BpElement beFD = GWT.create(FileDownloadForm.class);
		BpMenuButton bmFD = new BpMenuButton(beFD);
		bmFD.asWidget().setTitle("File Download");
		menuContainer.add(bmFD.asWidget());
		
		
		
//		RootPanel.get().add(be.asWidget());
		
//		userForm = new UserAdminForm();
//		RootPanel.get().add(userForm.asWidget());
//		userForm.showWindow();
	}
}
