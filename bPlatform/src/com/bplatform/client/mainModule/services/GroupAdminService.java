package com.bplatform.client.mainModule.services;

import com.bplatform.client.bpShared.DTO.BpGroupDTO;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("groupProperties")
public interface GroupAdminService extends RemoteService {
	
	/**
	 * Get Group Properties
	 */
	BpGroupDTO getGroupProperties(String groupId);
	
	/**
	 * ADD or Update Group
	 */
	String addOrUpdateGroup(BpGroupDTO setGroupProp) throws Exception;
	
	/**
	 * DELETE group
	 */
	String deleteGroup(String groupId) throws Exception;
}
