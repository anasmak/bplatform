package com.bplatform.client.mainModule.services;

import java.util.List;
import java.util.Set;

import com.bplatform.client.bpShared.DTO.BpUserGroupFoldersDTO;
import com.bplatform.client.bpShared.DTO.BpUserDTO;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("userProperties")
public interface UserAdminService extends RemoteService {
	BpUserDTO getUserProperties(String login);
	List<BpUserGroupFoldersDTO> getGroupsList() throws Exception;
	String setUserProperties(BpUserDTO setUserProp) throws Exception;
	Set<String> getUsersListFromGroup(String bpGroupId);
}
