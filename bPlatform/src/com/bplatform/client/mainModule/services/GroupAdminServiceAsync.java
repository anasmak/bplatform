package com.bplatform.client.mainModule.services;

import com.bplatform.client.bpShared.DTO.BpGroupDTO;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface GroupAdminServiceAsync {

	/**
	 * Get Group Properties
	 */
	void getGroupProperties(String groupId, AsyncCallback<BpGroupDTO> callback);
	
	/**
	 * ADD or Update Group
	 */
	void addOrUpdateGroup(BpGroupDTO setGroupProp,
			AsyncCallback<String> callback);

	/**
	 * DELETE group
	 */
	void deleteGroup(String groupId, AsyncCallback<String> callback);
	
}
