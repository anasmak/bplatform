package com.bplatform.client.mainModule.services;
import java.util.List;
import java.util.Set;

import com.bplatform.client.bpShared.DTO.BpUserGroupFoldersDTO;
import com.bplatform.client.bpShared.DTO.BpUserDTO;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface UserAdminServiceAsync {
	void getUserProperties(String login, AsyncCallback<BpUserDTO> callback)
					throws IllegalArgumentException;
	
	void getGroupsList(AsyncCallback<List<BpUserGroupFoldersDTO>> callback)
			throws IllegalArgumentException;

	void setUserProperties(BpUserDTO setUserProp,
			AsyncCallback<String> callback);
	
	void getUsersListFromGroup(String bpGroupId, AsyncCallback<Set<String>> callback)
			throws IllegalArgumentException;
}
