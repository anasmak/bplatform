/**
 * Sencha GXT 3.0.1 - Sencha for GWT
 * Copyright(c) 2007-2012, Sencha, Inc.
 * licensing@sencha.com
 *
 * http://www.sencha.com/products/gxt/license/
 */
package com.bplatform.client.mainModule.views.mockModel;

import com.sencha.gxt.data.shared.ModelKeyProvider;

public class NameImageModel implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	public NameImageModel() {
		
	}

public static ModelKeyProvider<NameImageModel> KP = new ModelKeyProvider<NameImageModel>() {
    @Override
    public String getKey(NameImageModel item) {
      return item.getName();
    }
  };

  private String name;
  private String image;

  public String getImage() {
    return image;
  }

  public NameImageModel(String name, String image) {
    this.name = name;
    this.image = image;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}
