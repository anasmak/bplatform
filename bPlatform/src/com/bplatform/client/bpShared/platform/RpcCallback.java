package com.bplatform.client.bpShared.platform;

import com.bplatform.shared.exceptions.AuthorizationException;
import com.bplatform.shared.exceptions.DuplicateNameException;
import com.bplatform.shared.exceptions.EmailValidationException;
import com.bplatform.shared.exceptions.EmptyGroupListException;
import com.bplatform.shared.exceptions.NameValidationException;
import com.bplatform.shared.exceptions.NoCustomPropertiesException;
import com.bplatform.shared.exceptions.NoLocalPropException;
import com.bplatform.shared.exceptions.PasswordNotMatchException;
import com.bplatform.shared.exceptions.PasswordValidationException;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.core.client.util.Format;
import com.sencha.gxt.widget.core.client.Dialog;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.box.MessageBox;
import com.sencha.gxt.widget.core.client.event.HideEvent;
import com.sencha.gxt.widget.core.client.event.HideEvent.HideHandler;
import com.sencha.gxt.widget.core.client.info.Info;

/**
 * Client side Exception controller. Used to control incoming exceptions and
 * alter client side in appropriate way.
 * 
 * @author Smak
 * 
 * @param <T>
 */
public abstract class RpcCallback<T> implements AsyncCallback<T> {
	
	private MessageBox mb = null;
	
	
	public RpcCallback() {}
	
	public RpcCallback(MessageBox mb) {
		this.mb = mb;
	}

	@Override
	public void onFailure(Throwable caught) {
		
		final HideHandler hideHandler = new HideHandler() {
			@Override
			public void onHide(HideEvent event) {
				Dialog btn = (Dialog) event.getSource();
		        String msg = Format.substitute("The '{0}' button was pressed", btn.getHideButton().getText());
		        Info.display("MessageBox", msg);
		    }
		};
		
		final AlertMessageBox d = new AlertMessageBox(null, null);
        d.addHideHandler(hideHandler);
		
        d.setHeadingText("Error");
		
		if (caught instanceof AuthorizationException) {
			d.setMessage("You are not authorized to view this!");
		} else if (caught instanceof EmailValidationException) {
			d.setMessage("Please, enter correct e-mail address");
		} else if (caught instanceof NameValidationException) {
			d.setMessage("Please, enter correct Name. Name must consist of more than three letters");
		} else if (caught instanceof PasswordNotMatchException) {
			d.setMessage("Password don't match. Please, retype passwords");
		} else if (caught instanceof PasswordValidationException) {
			d.setMessage("Please, enter correct password! Password must numeric symbol, BIG and small letters. Length must be within ranges 6..20");
		} else if (caught instanceof NoCustomPropertiesException){
			d.setMessage("Entered incorrect custom properties");
		} else if (caught instanceof DuplicateNameException){
			d.setMessage("This name exsist in database. Please, retype the Name");
		} else if (caught instanceof EmptyGroupListException){
			d.setMessage("There is no group exist. Please, add at least one Group");
		} else if (caught instanceof NoLocalPropException){
			d.setMessage("No Local properties");
		} else {
			d.setMessage("Something went wrong");						
		}
		
		if (mb!=null) {
			mb.hide();
		}
		d.show();
	}

	@Override
	public abstract void onSuccess(T result);

}
