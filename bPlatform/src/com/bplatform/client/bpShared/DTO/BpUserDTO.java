package com.bplatform.client.bpShared.DTO;

import java.io.Serializable;
import java.util.Set;

public class BpUserDTO implements Serializable{
	
	private static final long serialVersionUID = 5715054194445285928L;
	
	private String id;
	private String login;
	private String password;
	private String passwordRetype;
	private String fio;
	private String email;
	private Set<BpGroupDTO> groups;
	
	public Set<BpGroupDTO> getGroups() {
		return groups;
	}
	public void setGroups(Set<BpGroupDTO> groups) {
		this.groups = groups;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}	
	
	public String getPasswordRetype() {
		return passwordRetype;
	}
	public void setPasswordRetype(String passwordRetype) {
		this.passwordRetype = passwordRetype;
	}	
	
	public String getFIO() {
		return fio;
	}
	
	public void setFIO (String fio) {
		this.fio = fio;
	}
	
	public String getEmail () {
		return email;
	}

	public void setEmail (String email) {
		this.email = email;
	}
}
