package com.bplatform.client.bpShared.DTO;

import java.io.Serializable;
import java.util.List;

import com.sencha.gxt.data.shared.TreeStore;
import com.sencha.gxt.data.shared.TreeStore.TreeNode;

public class BpGroupDTO implements Serializable, TreeStore.TreeNode<BpGroupDTO> {

	  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private String groupId;
	private String parentId;
	
	public BpGroupDTO() {

	}

	public BpGroupDTO(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public BpGroupDTO(int id, String name, String groupId) {
		this.id = id;
		this.name = name;
		this.groupId = groupId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BpGroupDTO getData() {
		return this;
	}

	public List<? extends TreeNode<BpGroupDTO>> getChildren() {
		return null;
	}

	@Override
	public String toString() {
		return name != null ? name : super.toString();
	}
	
	public String getGroupId () {
		return groupId;
	}
	
	public void setGroupId (String groupId) {
		this.groupId = groupId;
	}
	
	public String getParentId () {
		return parentId;
	}
	
	public void setParentId (String parentId) {
		this.parentId = parentId;
	}

}